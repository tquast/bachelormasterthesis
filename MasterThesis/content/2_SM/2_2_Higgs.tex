\Subsection{Standard Model Higgs Boson}\label{sec:2.2}%
%
The Higgs mechanism was an ad'hoc extension to the SM in 1964 \cite{Higgs_mechanism} to account for the weak bosons' masses through spontaneous symmetry breaking.
An additional term in the general SM Lagrangian $\mathcal{L}_{Higgs}$ is added.
\begin{equation}
	\mathcal{L}_{Higgs}=\left(D_\mu \Phi\right)^\dagger\left(D^\mu\Phi\right)-V\left(\Phi^\dagger\Phi\right)~.
	\label{eq:LHiggs}
\end{equation}
$D_\mu$ is the covariant derivative defined in equation \ref{eq:LEW}. $\Phi$ is a $SU(2)$ doublet in which all possible Higgs fields are included. The potential $V$ is only expanded to quartic order in $\Phi$ to maintain renormalizability of the theory. Parameters $\mu$ and $\lambda$ in equation \ref{eq:HiggsPotential} are free and chosen to fit the data.
\begin{equation}
	V(\Phi^\dagger\Phi)=-\mu^2 \Phi^\dagger\Phi + \frac{\lambda}{2}\left(\Phi^\dagger\Phi\right)^2~.
	\label{eq:HiggsPotential}
\end{equation}	
\begin{wrapfigure}{r}{0.4\textwidth}
	\centering
	\includegraphics[width=0.30\textwidth]{figures/2_SM/2_3_Higgs/HiggsPotential_cite_higgsPot.png}
	\caption{Higgs potential for $\mu,\lambda > 0$ \cite{higgsPot}. $Re(\Phi)\hat{=}\Phi_1$ $Im(\Phi)\hat{=}\Phi_2$ is implied.}
	\label{fig:higgsPotential}
\end{wrapfigure}
Within the SM, $\Phi$ is treated as a doublet corresponding to the minimal configuration to break the symmetry in $V$: $\Phi=(\Phi_1,~\Phi_2)^T$. For both parameters $\mu^2$, $\lambda$ greater than zero, the potential's form resembles a "Mexican hat" (see figure \ref{fig:higgsPotential}). Spontaneous symmetry breaking refers to the fact that the ground state of $\Phi$ is invariant under rotation as long as $|\Phi|=\sqrt{\frac{\mu^2}{\lambda}}=:\nu$. Arbitrarily, the ground state can be noted as $\Phi = (0,~\nu)^T$. Excitation modes of the field are represented by the Higgs boson $H$. Then, excitations from the ground state can be written as in equation \ref{eq:HiggsExcitation}.
\begin{equation}
	\Phi = 
	\begin{pmatrix}
	0 \\
	\nu + \frac{H}{\sqrt{2}}
	\end{pmatrix}~.
	\label{eq:HiggsExcitation}
\end{equation}
Inserting the electroweak covariant derivatives on the excited field $\Phi$ around its ground state into equation \ref{eq:LHiggs}, the masses of the weak bosons are induced. Besides other parameters, they depend on the vacuum expectation $\nu$ (equation \ref{eq:WeakMassesHiggs}). Furthermore, coupling of the Higgs to vector bosons is founded.
\begin{equation}
	\begin{split}
		m_W^2&=\frac{1}{2}g^2\nu^2 ~,\\
		m_Z^2&=\frac{g^2\nu^2}{2\cos^2\theta_W} ~.
	\end{split}
	\label{eq:WeakMassesHiggs}
\end{equation}	
The mass of the Higgs boson is accessible through inserting the excited field $\Phi$ into the potential $V$. 
\begin{equation}
	V=-\frac{\mu^2\nu^2}{2} + \mu^2H^2 + \frac{\mu^2}{\sqrt{2}\nu}H^3 + \frac{\mu^2}{8\nu^2}H^4 ~.
	\label{eq:excitedHiggsPotential}
\end{equation} 
From equation \ref{eq:excitedHiggsPotential}, the tree level Higgs mass is given by equation \ref{eq:mH}.
\begin{equation}
	m_H = \sqrt{2}\mu ~.
	\label{eq:mH}
\end{equation}
While $\nu$ is determined through precision measurements of the well-established $m_W$ and $m_Z$, a direct handle on $\mu$ is only possible through the experimental determination of $m_H$. Figure \ref{fig:higgsMass} summarizes the results of corresponding analyses performed by the ATLAS and CMS collaborations in 2012. The combination of all results in a Higgs mass of $m_H=125.09\pm0.24$ GeV.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\textwidth]{figures/2_SM/2_3_Higgs/HiggsMass_cite_higgsMass.png}
	\caption{Experimental values for $m_H$ by ATLAS and CMS with the Run 1 data \cite{higgsMass}.}
	\label{fig:higgsMass}
\end{figure}
In analogy to vector bosons, the Higgs field is also used to explain the origin of mass of fermions. A Yukawa coupling term with constants $\lambda_f$ for each fermion like in equation \ref{eq:YukawaCoupling} is added to the Lagrangian \cite{SM_Lecture} to provide the mass terms in $\mathcal{L}_{QED}$ and $\mathcal{L}_{QCD}$ (equations \ref{eq:LQED} and \ref{eq:LQCD}).
\begin{equation}
	\mathcal{L}_{Yukawa}=-\lambda_f \left( \bar{\psi}_L\Phi\psi_R + \bar{\psi}_R\Phi\psi_L\right)~.
	\label{eq:YukawaCoupling}
\end{equation}
Inserting the excitation form for $\Phi$ (equation \ref{eq:HiggsExcitation}), one obtains fermion mass and fermion-Higgs interaction terms. The latter is given by equation \ref{eq:YukawaFermion} and implies a linear dependence of the coupling to the fermion's mass. Note that flavor changing Higgs vertices are not predicted in the SM.
\begin{equation}
	\mathcal{L}_{fermion-H}=-\lambda_f H \bar{\psi_f} \psi_f=-\frac{m_f}{\nu}H\bar{\psi_f} \psi_f~.
	\label{eq:YukawaFermion}
\end{equation}



Figure \ref{fig:higgsBR} shows a plot of the predicted branching ratios of the SM Higgs boson as a function of its mass. For $m_H=125$ GeV, the most dominant decay is to a pair of bottom quarks (58\%). W boson and tau lepton pairs are the second and third most probable decay channels with 21\% and 6.4\%, respectively.\newline
In proton-proton collisions, Higgs bosons are mainly produced in four processes \cite{topBR}: Fusion of gluon pairs with virtual top quark loops (ggF), fusion of vector bosons (W and Z) that are emitted from ingoing quarks (VBF), Higgsstrahlung in association with vector bosons (WH/ZH) and top quark associated production $t\bar{t}H$. Cross sections for a $\sqrt{s}=13$ TeV center-of-mass energy and a SM Higgs boson at $m_H=125$ GeV are listed below \cite{xsec_ttH}. The accuracies refer to the order in QCD and electroweak perturbation theory.

\begin{table}[H]
	\centering
	\caption{Higgs boson production channels and their SM cross section.}
	\begin{tabular}{c|c|c}
		\textbf{Process} & \textbf{Accuracy (QCD/EW)} & $\boldsymbol{\sigma}$ \textbf{[pb]} \\
		\hline
		\midrule
		ggF & N3LO/NLO & 48.58 \\
		VBF & NNLO/NLO & 3.782\\
		WH  & NNLO/NLO & 1.373\\
		ZH  & NNLO/NLO & 0.8839\\
		$\boldsymbol{t\bar{t}H}$ & \textbf{NLO/NLO }& \textbf{0.5071}\\
	\end{tabular}
\end{table}	

This thesis focusses on the $t\bar{t}H$ process in which the Higgs decays into a pair of bottom quarks and one W boson decays into leptons. It provides a direct probe of the suggested Yukawa coupling (eq. \ref{eq:YukawaFermion}) . Figure \ref{fig:ttHbbFeynman} shows a possible Feynman graph illustrating this process.
Despite its minor contribution to the total Higgs boson cross section, this production channel is experimentally distinguishable due to the presence of two heavy top quarks and their highly boosted decay products.

\begin{minipage}{0.45\textwidth}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{figures/2_SM/2_3_Higgs/HiggsBR_cite_higgsBR.png}
		\caption{Branching ratios of the SM Higgs boson for different masses of $m_H$ \cite{higgsBR}.}
		\label{fig:higgsBR}
	\end{figure}
\end{minipage}
\begin{minipage}{0.45\textwidth}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{figures/2_SM/2_3_Higgs/ttH_cite_CMSttH2012.png}
		\caption{Tree level Feynman diagram of the $t\bar{t}H$($b\bar{b}$) process in the semi-leptonic decay channel \cite{CMSttH2012}.}
		\label{fig:ttHbbFeynman}
	\end{figure}
\end{minipage}

