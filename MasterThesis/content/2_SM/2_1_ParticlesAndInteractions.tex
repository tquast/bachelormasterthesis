\Subsection{Matter and Interactions}\label{sec:2.1}
\textbf{Matter}\newline
In the SM, matter is made of fermionic particles with half integer spin. These are classified into three generations of quarks and leptons. In quantum field theory, particles are represented by physical fields $\Phi$ whose space-time coordinates $x_\mu$ are treated relativistically. Quantum numbers are assigned to characterize the particles. Those are discrete values indicating physical properties such as spin, electric charge, etc. Antimatter is treated equally within the SM framework and consists of antiparticles that correspond to the normal ones with identical mass but otherwise inverted quantum numbers. \newline

There are six different quark flavors. Up (u), charm (c) and top (t) quarks carry electric charge $+\frac{2}{3}e$ while down (d), strange (s) and bottom (b) quarks have $-\frac{1}{3}e$. Their masses range within five orders of magnitude among which the top quark's mass alone is two orders of magnitude above the bottom quark's, the second heaviest. 
All fundamental forces act on quarks. But, color confinement forbids their free, isolated existence in nature such that they only appear within bound states. On the contrary, leptons are only subject to electroweak interactions. Electrons (e), muons ($\mu$) and the heavy tau ($\tau$) leptons are electrically negatively charged. Neutrinos $\nu_e$, $\nu_{\mu}$ and $\nu_{\tau}$ constitute the remaining three leptons. They are very light, electrically uncharged and overall very hard to detect because they are only subject to weak interaction. \newline 
The left-hand side in figure \ref{fig:ParticleOverview} gives an overview on the fermions. Masses, their spin, electric charge and the grouping to generations are shown.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/2_SM/2_1_ParticlesAndInteractions/particleOverview_cite_pOvw.png}
	\caption{Overview on all particles within the SM \cite{pOvw}. Antiparticles of fermions are not shown.}
	\label{fig:ParticleOverview}
\end{figure}
\textbf{Interactions}\newline
Red-shaded boxes in figure \ref{fig:ParticleOverview} display force mediating gauge bosons. In the SM, these bosons carry integer spin and are considered as excitation modes of quantum fields. The Lagrange formalism constitutes the necessary mathematical framework. For this purpose, the most general Lagrange Density Function (Lagrangian) $\mathcal{L}$ that fulfills a set of given symmetries is formulated. More precisely, the SM is based on the $SU(3)\otimes SU(2) \otimes U(1)$ and Poincare symmetry groups. If the Lagrangian is constructed, the Euler-Lagrange equation (eq. \ref{eq:EulerLagrange}) provides the prescription to formulate equations of motions for the quantum fields $\Phi$ in four dimensional space with coordinates $x_\mu$.
\begin{equation}
	\partial_\mu \frac{\partial \mathcal{L}}{\partial(\partial_\mu \Phi)}=\frac{\partial \mathcal{L}}{\partial \Phi}~.
	\label{eq:EulerLagrange}
\end{equation}  
Solutions of the fields $\Phi$ cannot be fully solved analytically but only up to limited orders in perturbation theory. In this context, "Feynman diagrams were invented in 1948 to help physicists find their way out of a morass of calculations" \cite{FeynmanGraphs}. They visualize fundamental rules on how to compute matrix elements that are part of cross section predictions. \newline

The longest known interaction besides gravity is electromagnetism. Quantum electrodynamics (QED) has put this classical theory both into a relativistic and quantum physical context. Its Lagrangian is constructed from $U(1)$ symmetry considerations. Fermions are formulated as four-component spinors $\psi$. $\gamma_\mu$ are the Dirac matrices. For one particle with mass $m$ and charge $Q$, $\mathcal{L}_{QED}$ is given by equation \ref{eq:LQED}.
\begin{equation}
	\begin{split}
		\mathcal{L}_{QED}&=-\frac{1}{4}F^{\mu \nu}F_{\mu \nu}+\bar{\psi}\left(i\cdot D_\mu \gamma^\mu-m\right)\psi~, \\
		D_\mu &= \partial_\mu+i\cdot e A_\mu Q~, \\
		F_{\mu \nu}&=\partial_\mu A_\nu-\partial_\nu A_\mu~.
	\end{split}
	\label{eq:LQED}
\end{equation}

$A_\mu$ symbolizes the photon ($\gamma$) fields which are the carriers of electromagnetic force. They only couple to electrically charged particles and have no rest mass. In the framework of QED, Compton Scattering, as an example, is interpreted as shown in the Feynman diagrams in figure \ref{fig:ComptonFeynman}.\newline
Similarly, Quantum Chromodynamics (QCD) is constructed from $SU(3)$ considerations and constitutes the strong nuclear force. Three color charges as additional quantum numbers of quarks are introduced corresponding to nine gluons that mediate the strong force. Although they do not have rest mass, their range is limited to approximately 10$^{-15}$m. Therefore, the existence of a gluon with singlet colour state is not realized in nature. The QCD Lagrangian including $n_f$ quark ($q_j$) flavors with masses $m_j$ and eight gluon fields $g_\mu^A$ reads simlilar to $\mathcal{L}_{QED}$.
\begin{equation}
	\begin{split}
		\mathcal{L}_{QCD}&=-\frac{1}{4}\sum_{A=1}^{8}F^{A\mu \nu}F_{\mu \nu}^A+\sum_{j=1}^{n_f}\bar{q}_j\left(i\cdot D_\mu \gamma^\mu - m_j\right)q_j~,\\
		D_\mu &= \partial_\mu+i\cdot e_s \sum_{A=1}^{8}t^{A}g_\mu^A ~,\\
		F_{\mu \nu}^A&=\partial_\mu g_\nu^A-\partial_\nu g_\mu^A	-e_s C_{ABC}g_\mu^Bg_\nu^C ~.
	\end{split}
	\label{eq:LQCD}
\end{equation}
$SU(3)$ is a non-abelian group implying that its generators $t^A$ do not commute but fulfill the relation $[t^A,t^B]=i\cdot C_{ABC}\cdot t^C$, where $C_{ABC}$ stands for the complete asymmetric structure constants of $SU(3)$. As a consequence, the gluons couple to each other (see e.g. figure \ref{fig:GluonSelfFeynman}) resulting in a running coupling constant $\alpha_s=\frac{e_s^2}{4\pi}$. \newline
\begin{minipage}{0.49\textwidth}
	\begin{figure}[H]
		\includegraphics[width=\textwidth]{figures/2_SM/2_1_ParticlesAndInteractions/ComptonDiagram_cite_Nagashima.png}
		\caption{Compton Scattering described in QED \cite{Nagashima}. Curly lines represent photons, solid lines symbolize electrons.}
		\label{fig:ComptonFeynman}
	\end{figure}
\end{minipage}
\hfill
\begin{minipage}{0.34\textwidth}
	\begin{figure}[H]
		\includegraphics[width=\textwidth]{figures/2_SM/2_1_ParticlesAndInteractions/GluonSelfCoupling_cite_Nagashima.png}
		\caption{Gluon self-coupling \cite{Nagashima}.}
		\label{fig:GluonSelfFeynman}
	\end{figure}
\end{minipage}\newline
Namely, the coupling decreases with higher and increases with lower momentum transfers in the interaction. This explains the breakdown of QCD perturbation theory below some energy scale $\Lambda_{QCD}$. The current measurement for $\alpha_s$ at energies around 91 GeV (mass of Z boson) is $\alpha_s=0.1190\pm0.0026$. It is larger than the electromagnetic coupling $\alpha_{QED}=\frac{1}{137}$, thus justifying its name. Color confinement is a losely related concept. Unlike the electric force that vanishes at infinite distance $r$, the strong nuclear force between two colored particles does not vanish but tends to a constant. Eventually, the potential energy as shown for a quark-antiquark pair in equation \ref{eq:Vqq} \cite{lattice_QCD} reaches a point at which it is energetically preferred that massive particles emerge from the vacuum to form bound states with two quarks at close distance to each other. 
\begin{equation}
	V_{q\bar{q}}(r)\propto\left( \frac{\alpha_s(r)}{r}+...+\sigma \cdot r \right)~.
	\label{eq:Vqq}
\end{equation}
Last, W and the Z bosons are the mediators of the weak nuclear force. While the Z- is uncharged W bosons are either positively or negatively charged, hinting at a relation to electromagnetic force. Indeed, the theoretical unification of both interactions was a tremendous success. In Yang-Mills theory, leptons within one generation are ordered as doublets $\Psi=(l,~\nu_l)^T$. Empirically, one has found that only particles of same chirality interact weakly with each other. One defines $\Psi_{L/R}=\frac{1}{2}\left(1\mp\gamma_5\right)\Psi$ as the left and right handed chirality states. Then, the electroweak Lagrangian with neglected masses of the leptons reads according to equation $\ref{eq:LEW}$.
\begin{equation}
	\begin{split}
		\mathcal{L}_{EW}&=-\frac{1}{4}\sum_{A=1}^{3}F^{A\mu \nu}F_{\mu \nu}^A - \frac{1}{4}B^{\mu \nu}B_{\mu \nu}+\bar{\Psi}_Li\gamma^\mu D_\mu \Psi_L +  \bar{\Psi}_Ri\gamma^\mu D_\mu \Psi_R ~,\\
		F_{\mu \nu}^A&=\partial_\mu W_\nu^A-\partial_\nu W_\mu^A-g\epsilon_{ABC}W_\mu^B W_\nu^C ~,\\
		B_{\mu \nu} &= \partial_\mu B_\nu - \partial_\nu B_\mu ~,\\
		D_\mu &= \partial_\mu + i g \sum_{A=1}^{3}t^A W_\mu^A+i g' \frac{1}{2}Y B_\mu ~.
	\end{split}
	\label{eq:LEW}
\end{equation}
The covariant derivative $D_\mu$ includes the $SU(2)$ generators $t^A$ whose commutators are connected by the Levi-Cevita tensor $\epsilon_{ABC}$. Again, this group is non-abelian and self-interactions between the bosons are allowed. $Y/2$ is the $U(1)$ generator. $g$ and $g'$ correspond to two \textit{a-priori} independent coupling constants. $W_\mu^A$ and $B_\mu$ are hypothetical fields that form the physical W and Z bosons as well as the photon. $W^\pm$ are linear combinations of $W_\mu^{1/2}$. The electroweak mixing angle $\theta_W$ rotates $(W_\mu^3,~B_\mu)^T$ to form $(Z_\mu,~A_\mu)^T$. Additionally, it relates $g$ and $g'$. Through equation \ref{eq:thetaW}, $\theta_W$ is empirically determined.
 \begin{equation}
	 \begin{split}
		 A_\mu &= \cos \theta_W B_\mu + \sin \theta_W W_\mu^3 ~,\\
		 Z_\mu &= - \sin \theta_W B_\mu + \cos \theta_W W_\mu^3 ~,\\
		 g'&=g\cdot \tan \theta_W ~.
	 \end{split}
	 \label{eq:thetaW}
 \end{equation}
So far, the shown Lagrangian incorporates boson fields without mass terms implying that those carry no mass. While this is true for photons and gluons, the W and Z bosons are quite massive. In the next section, the Higgs field and related boson are thematised that preserve the underlying symmetries while giving mass to the weak bosons.