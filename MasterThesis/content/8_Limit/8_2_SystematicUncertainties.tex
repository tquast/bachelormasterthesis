\Subsection{Systematic Uncertainties}\label{sec:8.2}%
%
Systematic uncertainties are grouped into sources from which they originate. 
In general, either normalizations or entire shapes of observables are influenced. For the statistical inference, they are incorporated as nuisance parameters (see section \ref{sec:8.3}). Shape changing systematics may also include changes in the normalization. They require the generation of shifted templates of the observables. Typically, one recomputes the according distributions with $\pm1\sigma$ variations in these uncertainties.\newline
This passage briefly explains the systematics that are considered in this analysis. Experiment-related uncertainties are listed in \ref{sec:8.3.1}. Background predictions for the physical processes in proton-proton collisions at the Large Hadron Collider are given relative uncertainties in \ref{sec:8.3.2}. Those associated to general theory calculations are outlined in \ref{sec:8.3.3}. Appendix \ref{sec:appendixShapeUncertainties} extends this section and shows the impacts by some uncertainties on the observable in the 5j2b category.

\Subsubsection{Experimental Uncertainties}\label{sec:8.3.1}

\subsubsection*{B-Tag Reweighting}
Derivation of b-tag scale factors as outlined in section \ref{sec:7.1} is connected to a variety of systematic uncertainties. Light and heavy flavor purities in control samples are considered. Low amount of simulated events in some regions is accounted for by two independent polynomial extrapolations for light, heavy and charm flavored jets individually. In total eight independent uncertainties through recomputed scale factors are introduced.
Furthermore, they are derived for templates shifed by the uncertainty in the jet energy scale.
More details can be found in \cite{btagSFProcedure}.
Figure \ref{fig:btag_lf} depicts the impact of varied b-tag reweighting factors that account for the uncertainty in light flavor purity. In this example, the impact is rather flat meaning that the overall shape of the constructed observable in the 5j2b control region is not affected. 

\subsubsection*{Integrated Luminosity}
For the 2015 data taking period, the integrated luminosity is determined using the Pixel Cluster Counting \cite{cms_lumiDetermination} method. Van der Meer Scans \cite{cms_lumiDetermination} are performed to calibrate the scale. The suggested overall uncertainty on the luminosity measurement is 2.7 \% of which the proton bunch density correlations in its transverse dimensions and the reconstructed bunch separations are the dominating sources \cite{lumi15_error}. \newline It affects the rate of all processes in the analysis equally and simultaneously.

\subsubsection*{Heavy Flavour Matching}
Splitting of the $t\bar{t}$+Jets sample into separate classes is associated to uncertainties on the normalization of all involved processes except for $t\bar{t}~+~lf$. Conservatively, using the CMS GenHFHadronMatcher tool \cite{cms_GenHFHadronMatcher} a relative uncertainty of 50\% on the rate of $t\bar{t}$ + $b\bar{b}$/$b$/$2b$/$c\bar{c}$ is assumed. All of them are treated as independent nuisances. 

\subsubsection*{Jet Energy Scale}
Jet energy corrections and resolution factors (see section \ref{sec:4.3}) are connected to systematic uncertainties. Those are considered uncorrelated to each other in the analysis. Jet energies are recomputed with the varied factors, jet-based selection criteria are reapplied, all kinematic quantitites recomputed and events are reanalyzed. Missing transverse energy is corrected for the shifts using the CMS metCorrectionAndUncertainty tool \cite{cms_METTool}.
Despite modifying the overall kinematic situation of events and not the weight, boosted decision trees are not retrained for these shifts. Note that jet energy corrections shifts are fully correlated to the according uncertainty in the b-tagging reweighting. Figure \ref{fig:jec} shows the impact of up and down shifts in the jet energy corrections on simulation.

\begin{figure}[H]
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_3_SystematicUncertainties/btag_lf/eq5j_eq2b__D_BDT_ttH_eq5j.pdf}
		\caption{Impact of light flavor contaminations in the b-tag scale factor derivation on the constructed observable in 5j2b.}
		\label{fig:btag_lf}
	\end{minipage}
	\hfill
	\begin{minipage}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_3_SystematicUncertainties/jec/eq5j_eq2b__D_BDT_ttH_eq5j.pdf}
		\caption{Impact of the jet energy correction uncertainty on the constructed observable in 5j2b.}
		\label{fig:jec}
	\end{minipage}
\end{figure}

\subsubsection*{Muon Efficiencies}
Measurements of efficiencies in the muon selections are derived with uncertainties. Consequently, errors are attached to according scale factors for the trigger, for the ID and relative isolation working points separately. They depend on the muon's kinematic and influence both the rate and shape. Again, they are all treated as uncorrelated sources for systematic uncertainties.

\subsubsection*{Pileup}
The pileup reweighting procedure as discussed in section \ref{sec:7.1} depends on the probability for inelastic proton-proton interactions to deduce the number of primary vertices in one collision. Relative 5\% uncertainty is assumed on the predicted minimum bias cross section. Pileup weights are recomputed for the shifted scenarios and applied accordingly. 

\subsubsection*{Unclustered Energy}
In section \ref{sec:3.3.5} missing transverse energy (MET) is introduced as the negative vectorial sum in the transverse plane of all reconstructed objects by the particle flow algorithm. Nevertheless, energy deposits in the calorimeters cannot always be fully assigned to those objects. This leftover fraction is referred to as unclustered energy and can be propagated to MET in order to assess its uncertainty.
Since Run 2, relevant information is stored within the miniAOD format \cite{cms_METCorrections}. Missing transverse energy in an event is modified and the reconstruction algorithm is rerun for each shift.



\Subsubsection{Background Prediction}\label{sec:8.3.2}
\subsubsection*{Normalizations}

The expected yields of signal and background events are derived from cross section predictions at fixed order of perturbation theory. They affect the normalization of distributions and are split by physical processes. A distinction between QCD scale and parton density function related contributions is not made. Table \ref{table:xsec_uncertainties} lists the assumed relative uncertainties on the cross sections $\sigma_p$.

\begin{table}[H]
	\caption{Assumed relative uncertainties on cross sections in this analysis. $t\bar{t}H$ includes both $H\rightarrow b\bar{b}$ and $H\rightarrow other$.}
	\centering
	\begin{tabular}{c|c}
		Process $p$ & $\Delta \sigma_p / \sigma_p$ [\%]\\
		\hline
		\midrule
		$t\bar{t}H$ & 10\\
		$t\bar{t}$+Jets & 10\\
		$V$+Jets & 50\\
		$VV$+Jets & 50\\
		$t\bar{t}V$+Jets & 30\\
	\end{tabular}
	\label{table:xsec_uncertainties}
\end{table}	 


\subsubsection*{Multijet Contributions}
Like it is concluded in section \ref{sec:7.3.3}, an additional uncertainty originating from insufficient simulation of QCD mulitjet events is introduced in the 4j3b and 5j3b analysis categories. There, it impacts the normalization of all background templates through a relative uncertainty of 2\%.

\Subsubsection{Generator Related Uncertainties}\label{sec:8.3.3}
\subsubsection*{Parton Density Function}
 Parton density functions (pdf) in the proton are not fully predictable in theory but are accessible through measurements. Phenomenologically, fits to obtain pdfs depend on various assumptions, e.g. the underlying flavor scheme of quarks in the proton, their masses, etc. Thus, sets contain many replicas that account for the variety of assumptions.
 Many pdf sets have been measured and are provided by different collaborations. Here, simulated events are generated using one fixed set. In order to account for this circumstance, the PDF4LHC recommendation \cite{PDF4LHC_recommendations} is followed. In this context, events with momentum fractions $x_{1/2}$ in the hard interaction are reweighted towards the differential cross section that they would have if the used pdf set was different: $d\sigma'\propto pdf'\left(x_1\right)\cdot pdf'\left(x_2\right)$.\newline
 The CT14nlo pdf set \cite{CT14} is chosen as the shifted pdf set. Event weights are calculated like in equation \ref{eq:pdfWeight}.
 \begin{equation}
 \omega_{pdf}^r~=~\frac{pdf'_r\left(x_1\right)\cdot pdf'_r\left(x_2\right)}{pdf_0\left(x_1\right)\cdot pdf_0\left(x_2\right)} ~.
 \label{eq:pdfWeight}
 \end{equation}
 $x_{1/2}$ symbolizes the momentum fraction of the interacting partons in the hard process. $pdf'_r$ is pdf replica $r$ in the alternative pdf set while $pdf_0$ represents the nominal one. Up and down weights for a given event are finally computed as the average and standard deviation \cite{pdf_weight_calculation} over all replicas in the CT14nlo set. 
Pdf uncertainties are designed to affect the shape but not the normalization of distributions since the latter is already treated by the cross section tolerances. Therefore, the up and down weights are computed such that the total yield in simulation prior to any selection is conserved. 

\subsubsection*{Parton Showering}
Parton showering in simulation is based on phenomenological models. In those, certain parameters such as the strong coupling constant are subject to tuning since they are not accessible through fundamental theory calculations. To account for related uncertainties, additional samples (tables \ref{table:MCSamplesPSup} and \ref{table:MCSamplesPSdown}) are provided. This source of error is evaluated by rerunning the analysis with these shifted samples instead of the nominal ones. Again, the classifiers are not retrained. Figure \ref{fig:ps_scale} visualizes the impact of this uncertainty on the constructed observable in the 5j2b control region. Here, the error bands are not flat which indicates a shape changing effect on the observable's distribution.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/8_Limit/8_3_SystematicUncertainties/ps_scale/eq5j_eq2b__D_BDT_ttH_eq5j.pdf}
	\caption{Impact of the parton showering model uncertainty on the constructed observable in 5j2b.}
	\label{fig:ps_scale}
\end{figure}

\subsubsection*{Renormalization and Factorization Scale}
Within the nominal matrix element ($M_0$) calculation, two arbitrary scales must be introduced that are required due to the limited order in perturbation theory. 
The renormalization scale $\mu^R$ is introduced to circumvent ultraviolet divergence in higher order loop integrals. In contrast, a factorization scale $\mu^F$ is necessary to avoid infrared divergence that occur when initial state splitting is incorporated in the calculations \cite{scale_explanation}.\newline
Both scales are independent from each other and their choice influence the computed differential cross section of generated events. Consequently, event weights similar to the parton density function related uncertainties are introduced that cover the deviances in matrix elements. Both scales are varied independently and the two combinations forming the largest envelope in the relevant observables are chosen \cite{meScale_recommendationForShifts}. 
In a separate analysis note \cite{MEScale_shifts} it has been established that this is the case for simultaneous up and down variation of both scales. Hence, the weights are derived according to equation \ref{eq:me_scale_weights}.
\begin{equation}
	\begin{split}
		\omega_{M}^{up/down}~\propto~\frac{|M_{up/down}|^2}{|M_0|^2}  ~,\\
		M_{up/down}~=~M\left(\mu_{up/down}^R,\mu_{up/down}^F\right) ~.
	\end{split}
	\label{eq:me_scale_weights}
\end{equation}
Finally, the proportionality indicates that $\omega_{ME}^{up/down}$ are also modified such that the total yield in simulation prior to any selection is conserved.
