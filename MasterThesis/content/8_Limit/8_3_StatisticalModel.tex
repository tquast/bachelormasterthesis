\Subsection{Statistical Inference with the Asymptotic CLs Method}\label{sec:8.3}%
%
The goal of this section is the motivation of and an overview on the applied statistical method to determine an upper limit on the $t\bar{t}H(b\bar{b})$ cross section $\sigma$ at 95 \% confidence level. 
For this purpose, the measured cross section is scaled to a signal strength paramer $\mu$ which is defined as its ratio to the Standard Model prediction (equation \ref{eq:signalStrength}). Note that the Higgs decays into other objects is not included such that $\sigma \neq \sigma_{t\bar{t}H}$.
\begin{equation}
	\mu~:=~\frac{\sigma}{\sigma_{SM}} ~.
	\label{eq:signalStrength}
\end{equation}

The probability for the signal strength parameter to take some value given the data is of physical interest. Its answer cannot be deduced in a frequentist manner since a model, namely the signal strength at some value, is not a random variable within repeated conductions of an experiment. But, if no assumptions neither on the probability to obtain the recorded data nor on the prior distribution of $\mu$ are made, the likelihood to establish a signal model at strength $\mu$ in data is proportional to recording this data if the signal model at strength $\mu$ is true. The necessary inversion is provided by a subjective variant of Bayes' theorem like in equation \ref{eq:subjectiveBayes}.
\begin{equation}
	\begin{split}
		&P(\mu|data)~=~\frac{P(data|\mu)\cdot P(\mu)}{P(data)} \\
		&\Rightarrow	P(\mu|data)~\propto ~P(data|\mu) ~.
	\end{split}
	\label{eq:subjectiveBayes}
\end{equation}  
By implication, the following two questions are equivalent:
\begin{enumerate}
	\item What signal strength parameters $\mu$ can be excluded at 95\% confidence given the data?
	\item For what values of the signal strength parameter $\mu$ does the data represent fluctuation to the assumed signal model with less than 5\% probability?
\end{enumerate}
Here, the second question is addressed using a modified frequentist approach, also known as $CLs$. $CLs$ was first introduced in the Higgs searches at LEP \cite{LEP_HiggsSearch}. The method is suited if a conducted experiment lacks sensitivity to the signal. Exclusions of hypotheses for which the measurement is not sensitive to are not as easily rejected e.g. if the recorded data by chance show downward fluctuations with respect to the null hypothesis \cite{LHC_Statistics}. 
The procedure in its asymptotic form is applied in the statistical inference and is briefly explained in the following. References \cite{LHCCLsHiggs} and \cite{asymptoticCLs} provide further details.\newline

Equation \ref{eq:LStatmodel} introduces the likelihood function $L$ that relates recorded data to the model under test.
\begin{equation}
	\begin{split}
	L(data|\mu,\theta)&:=Poisson(data|\mu\cdot s(\theta)+b(\theta))\cdot p(\widetilde{\theta}|\theta)\\
	&=\prod_{i=1}^{N_{bins}}\frac{(\mu \cdot s_i(\theta)+b_i(\theta))^{n_i(\theta)}}{n_i(\theta) !}e^{-\mu\cdot s_i(\theta)-b_i(\theta)}\cdot p(\widetilde{\theta}|\theta) ~.
	\end{split}
	\label{eq:LStatmodel}
\end{equation}
The product is performed over all bins in the constructed observable's template. $s_i$ denotes the predicted amount of signal events in bin $i$, $b_i$ accordingly of backgrounds and $n_i$ the actual recorded data yield. $\theta$ represents the full set of nuisance parameters. Those are used to incorporate systematic uncertainties (see table \ref{table:nuisances}). The dependence of yields $s_i,~b_i,n_i$ in fixed bins on the nuisances $\theta$ is explicitly noted. $p(\widetilde{\theta}|\theta)$ reflects the degree of belief of what the nuisances could be with respect to a default value $\widetilde{\theta}$. For instance, Gaussian priors centralized at the nominal value of each nuisance are often chosen. \newline
Compatibility between data and the model under test is quantified by the profile likelihood ratio test statistic $\widetilde{q_\mu}$ (equation \ref{eq:testStatistics}). Its definition is motivated by the Neyman-Pearson lemma which states that the profile likelihood ratio is the most powerful test for a hypothesis test between two simple hypotheses.
\begin{equation}
	\begin{split}
		\widetilde{q}_\mu&:=
		\begin{cases}
			-2\ln{\frac{L(data|\mu,\hat{\theta}_\mu)}{L(data|\hat{\mu},\hat{\theta})}}&, 0\leq\hat{\mu} \leq \mu\\
			0              &, \text{otherwise} 
		\end{cases}  ~,\\
		\hat{\theta}_\mu&: \text{critical values that maximize $L$ given $\mu$ under test}  ~,\\
		\hat{\theta}&: \text{critical values that maximize $L$}  ~,\\
		\hat{\mu}&: \text{critical value that maximizes $L$}  ~.\\
	\end{split}
	\label{eq:testStatistics}
\end{equation}
The concrete values for $\hat{\theta}_\mu$, $\hat{\theta}$ and $\hat{\mu}$ are obtained by high dimensional maximizations of the likelihood function. Good agreement between data to the model under test at signal strength $\mu$ results in $\widetilde{q_\mu}$ close to zero. Forcing $\mu \geq 0$ constitutes the physical constraint of positive cross sections\footnote{Some signals result in decreasing event yields in some phase space regions. This does not apply for $t\bar{t}H(b\bar{b})$.}. By design, $\mu \geq \hat{\mu}$ guarantees a one-sided interpretation of the determined limit. The same condition implies that upper fluctuations of data are not used to reject signal strength hypotheses at values lower to the one maximizing $L$. \newline
With the data at hand and the hypothesis $\mu$ to test, one calculcates the observed likelihood ratio $\widetilde{q}_\mu^{obs}$. In addtion, the sets of nuisance parameters that maximize the likelihood under condition $\mu$ as tested and $\mu=0$ are determined. Both are needed for the construction of two probability density functions for $\widetilde{q}_\mu$ separately given the signal and background (s+b) $f(\widetilde{q}_\mu|\mu,\hat{\theta}_\mu^{obs})$ and background-only (b) hypotheses $f(\widetilde{q}_\mu|0,\hat{\theta}_0^{obs})$. Eventually, these functions are used to interprete $\widetilde{q}_\mu$ in a probabilistic context. \newline	
Concerning their construction, either many toy datasets are generated following s+b and b or the concept of Asimov Data, i.e. one data set in which all templates are set to their expectations \cite{atlas_WaldTheorem}, is applied. The latter procedure is based on the Wald Theorem. It provides an analytic form for the distribution of test statistic $\widetilde{q}_\mu$ if the data sample size $N$ is sufficiently large in order to neglect $\mathcal{O}(1/\sqrt{N})$ terms. In this asymptotic limit, $f(\widetilde{q}_\mu|\mu',\hat{\theta}_{\mu'})$ takes an analytic form whereby necessary parameters are computed from a covariance matrix $V_{jk}$ whose inverse is given by equation \ref{eq:covarianceLA}.
\begin{equation}
V_{jk}^{-1}=-\frac{\partial^2\ln L_A}{\partial \theta_j \partial \theta_k}~,~~j,k \in \mathbb{N}  ~.
\label{eq:covarianceLA}
\end{equation}
In this notation, $L_A$ corresponds to the Asimov likelihood in which the dataset is set to its expectation and $\theta_0=\mu$. 
Figure \ref{fig:CLsPdfs} illustrates exemplary probability density functions for $\widetilde{q}_\mu$ that are derived from toy data. The observed value $\widetilde{q}_\mu^{obs}$ is labelled. There, a signal at strength $\mu=1$ in red is tested against the background-only hypothesis in blue.\newline
Afterwards, the p-values, i.e. the probability to observe data that deviate to the tested model compared equally or more than the recorded data, for the s+b to the b-only hypotheses are calculated following equation \ref{eq:pValues}.
\begin{equation}
	\begin{split}
		p_\mu&=\int_{\widetilde{q}_\mu^{obs}}^{\infty} f(\widetilde{q}_\mu|\mu,\hat{\theta}_\mu^{obs})~d\widetilde{q}_\mu ~,\\
		1-p_0&=\int_{\widetilde{q}_\mu^{obs}}^{\infty} 	f(\widetilde{q}_\mu|0,\hat{\theta}_0^{obs})~d\widetilde{q}_\mu ~.
	\end{split}
	\label{eq:pValues}
\end{equation}



\begin{minipage}{0.49\textwidth}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/8_Limit/8_2_StatisticalModel/pdfPlot_cite_LHCCLsHiggs.png}
	\caption{Model $f(\widetilde{q}_\mu|\mu',\hat{\theta}_{\mu'}^{obs})$ for $\mu'=1$ and $\mu'=0$. In this particular example, pdfs are estimated using toy data \cite{LHCCLsHiggs} whereas asymptotic formulas are used in this thesis.}
	\label{fig:CLsPdfs}
\end{figure}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\begin{figure}[H]
		\label{fig:qmuDistribution}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_2_StatisticalModel/Expected_cite_asymptoticCLs.png}
		\caption{Distribution of $\mu_{95\%}$ for many toy datasets. Median and the corresponding one and two Gaussian standard deviations are shaded and compared to the result from the analytic approximation using Asimov data \cite{asymptoticCLs}.}
		\label{fig:expectedLimit}
	\end{figure}
\end{minipage} \vspace{5mm}\newpage
Finally, each hypothesis $\mu$ is attributed one $CLs$ which is defined as the ratio of p-values (equation \ref{eq:CLs}).
\begin{equation}
CLs:=\frac{p_\mu}{1-p_0} ~.
\label{eq:CLs}
\end{equation}
The entire procedure is iterated while $\mu$ under test is varied. One stops when  $CLs(\mu)=0.05$ is reached. That particular value $\mu$ dictates $\mu_{95\%}$. It is considered the \textit{observed} upper limit on the signal strength at 95\% $CLs$ confidence level. The result is rather conservative in the sense that $CLs$ is known for computing upper limits greater than those obtained with comparable methods.\newline

\textit{Expected} limits serve as quantification for the \textit{sensitivity} of an experiment to find some signal. They are used when the analysis is still blinded and being optimized. For this purpose, $\mu_{95\%}$ is computed for many toy experiments in which data is generated with respect to the background-only model. This way, many upper limits are obtained. An exemplary $\mu_{95\%}$ distribution is shown in figure \ref{fig:expectedLimit}. Usually, one quotes its median as the sensitivity and the borders of the 68\% band (green shade) as its error. Alternatively, in order to save computing resources, Asimov data again provide an analytic approximation in the asymptotic data size limit. Blue dashed lines in the figure display its approximated median and according one and two Gaussian standard deviations. They agree with those as obtained through toy data generation. In summary, sensitivity corresponds to the expectance of $\mu_{95\%}$ if the background-only hypothesis is true. An observed limit that is significantly greater than this expectance is taken as evidence for the true existence of the signal. Vice versa, observed limits significantly below are used to reject the true signal's existence. 
Actual measurements of $\mu$ are usually performed if the experiment's sensitivity is close to one.\newline

Nuisance parameters in this analysis and their correspondence to systematic uncertainties are provided in table \ref{table:nuisances}. They are grouped into rate and template shape changing parameters. Splitting of generator related uncertainties into mutliple and independent nuisances is recommended by the official Higgs combination convention \cite{Higgs_comb_convention}. \newline Furthermore, in order to account for the limited number of simulated events, two additional templates are provided for each process and each bin. In those the uncertainty on predicted yields is added to or subtracted from the nominal value.\newpage
\begin{table}[H]
	\caption{List of nuisance parameters in the statistical model and the systematic uncertainty to which they correspond.}
	\footnotesize
	\centering
	\begin{tabular}{c|c|cl}
		\textbf{Source}&\textbf{Effect}& \textbf{Name} & \textbf{Systematic uncertainty}\\
		\hline
		\midrule
		\multirow{19}{*}{\rotatebox[origin=c]{90}{experiment}} & shape & jec & \pbox{20cm}{jet energy correction}\\
		& shape & jer & \pbox{20cm}{jet energy resolution}\\
		& shape & uncl\textunderscore en & \pbox{20cm}{unclustered energy}\\
		& shape & pileup & \pbox{20cm}{pileup}\\
		& shape & mu\textunderscore id & \pbox{20cm}{muon ID selection}\\
		& shape & mu\textunderscore iso & \pbox{20cm}{muon isolation selection}\\
		& shape & mu\textunderscore trig & \pbox{20cm}{muon trigger}\\
		& shape & btag\textunderscore lf & \pbox{20cm}{light flavor contamination in b-tag reweighting}\\
		& shape & btag\textunderscore lfStats1 & \pbox{20cm}{1st degree polynomial extrapolation for low yields\\in light flavor regions in b-tag reweighting}\\
		& shape & btag\textunderscore lfStats2 & \pbox{20cm}{2nd degree polynomial extrapolation for low yields\\in light flavor regions in b-tag reweighting}\\
		& shape & btag\textunderscore hf & \pbox{20cm}{heavy flavor contamination in b-tag reweighting}\\
		& shape & btag\textunderscore hfStats1 & \pbox{20cm}{1st degree polynomial extrapolation for low yields\\in heavy flavor regions in b-tag reweighting}\\
		& shape & btag\textunderscore hfStats2 & \pbox{20cm}{2nd degree polynomial extrapolation for low yields\\in heavy flavor regions in b-tag reweighting}\\
		& shape & btag\textunderscore cStats1 & \pbox{20cm}{1st degree polynomial extrapolation for low yields\\in charm flavor regions in b-tag reweighting}\\
		& shape & btag\textunderscore cStats2 & \pbox{20cm}{2nd degree polynomial extrapolation for low yields\\in charm flavor regions in b-tag reweighting}\\
		& lnN & lumi & \pbox{20cm}{luminosity}\\ 
		& lnN & tthf\textunderscore ttbb & \pbox{20cm}{branching ratio of $t\bar{t}$+Jets to $t\bar{t}+b\bar{b}$}\\
		& lnN & tthf\textunderscore tt2b & \pbox{20cm}{branching ratio of $t\bar{t}$+Jets to $t\bar{t}+2b$}\\
		& lnN & tthf\textunderscore ttb & \pbox{20cm}{branching ratio of $t\bar{t}$+Jets to $t\bar{t}+b$}\\
		& lnN & tthf\textunderscore ttcc & \pbox{20cm}{branching ratio of $t\bar{t}$+Jets to $t\bar{t}+c\bar{c}$}\\
		\midrule
		\multirow{7}{*}{\rotatebox[origin=c]{90}{\pbox{20cm}{background \\  prediction}}} & lnN & xsec\textunderscore ttH & \pbox{20cm}{cross section uncertainty on the $t\bar{t}H$ process}\\
		& lnN &  xsec\textunderscore tt& \pbox{20cm}{cross section uncertainty on the $t\bar{t}$+Jets processes}\\
		& lnN &  xsec\textunderscore ST& \pbox{20cm}{cross section uncertainty on the Single $t/\bar{t}$ processes}\\
		& lnN &  xsec\textunderscore VJets& \pbox{20cm}{cross section uncertainty on V+Jets processes}\\
		& lnN &  xsec\textunderscore VVJets& \pbox{20cm}{cross section uncertainty on VV+Jets processes}\\
		& lnN &  xsec\textunderscore ttVJets& \pbox{20cm}{cross section uncertainty on $t\bar{t}$V+Jets processes}\\
		& lnN & QCD & \pbox{20cm}{additional multijet contributions in the 4j3b and 5j3b categories}\\
		\midrule
		\multirow{7}{*}{\rotatebox[origin=c]{90}{\pbox{20cm}{generator \\ related}}} & shape & pdf\textunderscore ttH & \pbox{20cm}{pdf uncertainty on the $t\bar{t}H$ process}\\
		& shape & pdf\textunderscore gg & \pbox{20cm}{pdf uncertainty on gluon induced processes}\\
		& shape & pdf\textunderscore qq & \pbox{20cm}{pdf uncertainty on quark-antiquark induced processes}\\
		& shape & pdf\textunderscore gq & \pbox{20cm}{pdf uncertainty on gluon-quark induced processes}\\
		& shape & me\textunderscore scale\textunderscore ttH & \pbox{20cm}{scale uncertainty for the $t\bar{t}H(b\bar{b})$ process}\\
		& shape & me\textunderscore scale\textunderscore tt & \pbox{20cm}{scale uncertainty for the processes including top quarks but $t\bar{t}H$}\\
		& shape & ps\textunderscore scale & \pbox{20cm}{uncertainty on the parton showering model}\\
		\midrule
		\end{tabular}
	\label{table:nuisances}
\end{table}
\newpage