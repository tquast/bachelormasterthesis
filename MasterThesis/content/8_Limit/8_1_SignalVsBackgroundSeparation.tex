\Subsection{Signal vs. Background Separation}\label{sec:8.1}
The construction of signal-to-background separating observables in the analysis categories is addressed in this section. First, the concept of boosted decision trees is explained in \ref{sec:8.1.1}. Input variables to the classifiers are listed in \ref{sec:8.1.2} and their trainings are validated in \ref{sec:8.1.3}. Subsequently, final observables are selected based on their ROC integrals in \ref{sec:8.1.4}.


\Subsubsection{Boosted Decision Trees}\label{sec:8.1.1}
Boosted decision trees (BDTs) are a multivariate analysis concept to separate input data into different classes. First applications of BDTs in high-energy physics were tested during studies at the MiniBooNE experiment in 2004 \cite{BDT_principle}. 
CMS has first adopted the algorithm in its single top cross section measurement with proton-proton collision data recorded in 2011 \cite{cms_singleTopXSec}. Latest since then, BDTs are often used as a classification algorithm in high energy physics analyses.
\newline Proper variables that tend to yield different distributions for distinct classes in the input data are ideally given. Samples are first split into training and testing sets. The latter is used for a subsequent validation of the constructed trees.\newline
In general, boosted decision trees comprise an ensemble of individual decision trees that are constructed iteratively. In between two constructions, boosting, i.e. the modification of certain event weights $W_i$, is performed. In high energy physics, $W_i$ are often initialized as the luminosity normalization weights defined by equation \ref{eq:lumiweight}. \newline
In the following, the procedure is explained for two-class classification of events into either signals or backgrounds. At the beginning, the construction of one decision tree, the algorithm consecutively checks which variable separates best between signal and background in terms of purity $P$ as defined in equation \ref{eq:Purity}. 
\begin{equation}
P~=~\frac{\sum_{s}^{}W_s}{\sum_{s}^{}W_s+\sum_{b}^{}W_b} ~.
\label{eq:Purity}
\end{equation}
Letters s and b indicate that the sums are taken over all signal and background events respectively in the node. 
The Gini-Index (equation \ref{eq:Gini}) is defined for every branch. It quantifies the power of separation of hypothetical criterion thresholds on the input parameter space. A cut is applied and the set is divided into two branches. The first branch is made up of all events that fulfill the cut while the other contains the remaining events.
Let $N$ be the number of events that are sorted into the branch in question.
\begin{equation}
G~=~P\cdot\left(1-P\right)\cdot\left(\sum_{i=1}^{N}W_i\right) ~.
\label{eq:Gini}
\end{equation}
Ultimately, the cut that minimizes the sum of Gini indexes of the two resulting branches is chosen. Those cuts that either result in low or high purities, thus minimizing $G$, are favored. The tree is built while this pattern is iterated at all nodes until a given number of branches or a fixed amount of cuts is reached. Figure \ref{fig:BDTPrinciple} sketches the architecture of a decision tree obtained through this procedure. \newline
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/BDT_cite_BDT_principle.png}
	\caption{Sketch of a simple decision tree with signal (background) event dominated nodes S (B) \cite{BDT_principle}. Illustrated variables and thresholds are examples only.}
	\label{fig:BDTPrinciple}
\end{figure}
Here, a leaf, i.e. a node at the very bottom, is called a background leaf if its purity is smaller than a defined value. Otherwise it is called a signal leaf. It results in an absolute classification of events. An event is considered signal-like if it falls into a signal leaf and vice versa for backgrounds.
There is no handle on the robustness of this scheme because the training of one single decision trees tends to be unstable. As an example, small changes in the training sample may have huge effects on the tree's final structure. \newline 
For this reason, multiple decision trees are constructed iteratively. The concept of \textit{boosting} is applied and implies the increase of misclassified events' weights in between two iterations. 
For instance, the AdaBoost method \cite{adaboost} increases the weights of misclassified events from iteration $m$ to $m+1$ by $W_i^{m+1}\propto W_i^{m}\cdot e^{\alpha_m}$ with $\alpha_m$ as defined in equation \ref{eq:alphaM} while keeping those of correctly identified events constant (besides some normalization).
\begin{equation}
	\begin{split}
		\alpha_m~&=~\beta\cdot \log\left( \frac{1-\epsilon_m}{\epsilon_m}\right) ~,\\
		\epsilon_m~&:~\text{error rate by the decision tree at iteration $m$},~\epsilon_m < \frac{1}{2} ~,\\
		\beta~&:~\text{free tuning parameter} ~.
	\end{split}
	\label{eq:alphaM}
\end{equation}
Then, the decision tree is rebuilt various times always comprising the updated weights. For each tree, events either get a score $T_m(x_i)=+1$ when ending up in a signal leaf or accordingly $T_m(x_i)=-1$ when being sorted into a background leaf. Finally, after $N_{trees}$ repetitions, a final classifier value is computed through the $\alpha_m$ weighted sum over the scores from all decision trees $T(x_i) \propto \sum_{m=1}^{N_{trees}}T_m(x_i)\cdot \alpha_m$. 
At the end, a value close to 1 yields signal-like events while values close to -1 are rather background-like.\newline 
Overtraining refers to the lack of generalization by the decision trees during the training. 
Then, the ensemble of trees adapts itself to noise in the underlying set rather than on the overall structure \cite{adaboost}. Thus, the term overfitting is often used synonymously. Typically, it occurs when the amount of events for training is not sufficiently large or the settings are not ideal. A common criterion to identify its occurence is the comparison of discriminator distributions in the training and testing samples. In case of overtraining, both distributions are not compatible which can be quantified through dedicated tests.

\Subsubsection{Input Variables}\label{sec:8.1.2}
In total, twelve boosted decision trees are trained. Trainings are performed both in the nine analysis categories (see figure \ref{fig:Categories}) and in three inclusive jet multiplicity bins (4j, 5j, $\geq$6j).
For each, a set of input variables is defined. This section focusses on their definitions. The overview on assignments to the trainings is given by table \ref{table:BDTInputs} in appendix section \ref{sec:appendixInputs}. 
There, signal to background separation and the modelling of certain variables for the 6j3b category are also shown.
Despite providing good discrimination in simulation, quantitities related to charge information are not included due to their rather bad agreement between simulation and recorded data.
\begin{itemize}
	\item \textbf{jet\textunderscore sumPt:} Scalar sum of transverse momenta over all $N_{Jet}$ selected jets in the event. 
	\begin{align*}
		\sum_{i=1}^{N_{Jet}} p_{t,i} ~.
	\end{align*}
	\item \textbf{jet\textunderscore minDeltaRLepton:} Minimum angular distance of all jets to the signal muon.
	\begin{align*}
		\begin{split}
		\min\left( \Delta R_{i, \mu} \right),~\forall ~i \in \{1,2,...,N_{Jet}\}  ~,\\
		\Delta R_{i,\mu}=\sqrt{ \Delta \phi_{i,\mu}^2 + \Delta \eta_{i,\mu}^2 } ~.
		\end{split}
	\end{align*}
	\item \textbf{jet\textunderscore csv\textunderscore mean:} Average combined secondary vertex value for all selected jets.
	\begin{align*}
		\frac{1}{N_{Jet}}\cdot \sum_{i=1}^{N_{Jet}} csv_i ~.
	\end{align*}
	\item \textbf{jet\textunderscore rapidity:} The sphericity tensor $S_{\alpha,\beta}$ is calculated following equation \ref{eq:sphericityTensor} \cite{jet_shapes}. The sum is computed over all selected jets in the event.
	\begin{equation}
		S_{\alpha,\beta}~=~\frac{\sum_{i=1}^{N_{Jet}} p_{\alpha,i}\cdot p_{\beta,i} }{\sum_{i=1}^{N_{Jet}} |\vec{p_{i}}|^2} ~.
		\label{eq:sphericityTensor}
	\end{equation} Its three eigenvalues $\lambda_1\geq\lambda_2\geq\lambda_3$ define the sphericity $S$.
	\begin{align*}
		S=\frac{3}{2\left( \lambda_2+\lambda_3 \right)} ~.
	\end{align*} 
	\item \textbf{jet\textunderscore aplanarity:} Similar to sphericity, aplanarity is defined as $\frac{3}{2\cdot \lambda_3}$ \cite{jet_shapes}.
	\item \textbf{bjet$\boldsymbol{<}$n$\boldsymbol{>}$\textunderscore BJECFactor:} Additional energy correction factor for b-tagged jets evaluated by the method described in section \ref{sec:6}. n $\in \{1,2\}$ denotes the ranking of jets with respect to their $csv$ value.
\end{itemize}

The variables below are obtained from the $\chi^2$ based reconstruction method in section \ref{sec:5}. Here, n consistently stands for the underlying hypothesis.
\begin{itemize}
	\item \textbf{mtlep\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Reconstructed mass of the top quark with leptonically decaying W boson.
	\item \textbf{deltaRHbb\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Angular distance $\Delta R_{bH, bbarH}$ of the two jets that are assigned to the decay of the Higgs boson.
	\item \textbf{mH\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Reconstructed mass of the Higgs boson.
	\item \textbf{mWhad\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Reconstructed mass of the hadronically decaying W boson.
	\item \textbf{deltaRj1j2\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Angular distance $\Delta R_{j1, j2}$ of the two jets that are assigned to the decay of the hadronically decaying W boson.
	\item \textbf{mthad\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Reconstructed mass of the top quark with hadronically decaying W boson.
	\item \textbf{pttbar\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$}: Transverse momentum of the reconstructed $t\bar{t}$ system.
	\item \textbf{deltaEtaTHadHiggs\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$:} Difference in pseudorapidity $\Delta \eta_{H,t_{had}}$ between the reconstructed Higgs boson and top quark with hadronically decaying W boson.
	\item \textbf{pttH\textunderscore h11:} Transverse momentum of the reconstructed $t\bar{t}H$ system.
	\item \textbf{chi2\textunderscore tot\textunderscore h$\boldsymbol{<}$n$\boldsymbol{>}$\textunderscore p1:} $\chi^2$ score as defined by equation \ref{eq:chi2} for the selected permutation in the reconstruction.
	\end{itemize}

\Subsubsection{Performance of the Classifier}\label{sec:8.1.3}
The dedicated half of simulated events for the classifier construction is subdivided further. 60\% are used for its training while 40\% serve the subsequent test for overtraining. 
The Toolkit for Multivariate Analysis (see section \ref{sec:3.4}) which is an extension to the ROOT framework is used for implementation and training of the boosted decision trees.
Event weights are initialized to their luminosity normalization (equation \ref{eq:lumiweight}). 400 decision trees are built whose maximum number of constructed cuts is set to 20. Separation in each node is quantified by the Gini-index (equation \ref{eq:Gini}). Pruning \cite{pruning}, a method that removes sections of the tree that are not relevant for generalization towards the interesting representations, is not applied. The free boosting parameter (equation \ref{eq:alphaM}) is set to $\beta=0.2$. \newline
Overtraining is checked through the comparison of classifier distributions obtained from the training and test samples. The Kolmogorov-Smirnov (KS) test is the preferred method to determine if two histograms differ significantly from each other since it does not make any assumptions of the underlying true distribution.
Trainings for which the KS tests for signal or background shapes are close to zero are likely to be affected by overtraining. Hence, they are not considered for the selection of final observables.

\begin{figure}[H]
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/overtraining__eq4j.pdf}
		\caption{Test of overfitting in the $N_{Jet}=4$ training.}
		\label{fig:Overtraining4}
	\end{minipage}
	\hspace*{\fill}
	\begin{minipage}{0.45\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/overtraining__eq5j_eq3b.pdf}
		\caption{Test of overfitting in the $N_{Jet}=5,~N_{tag}=3$ training.}
		\label{fig:Overtraining53}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/overtraining__ge6j.pdf}
		\caption{Test of overfitting in the $N_{Jet}\geq 6$ training.}
		\label{fig:Overtraining6}
	\end{minipage}
	\hspace*{\fill}
	\begin{minipage}{0.45\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/overtraining__ge6j_eq3b.pdf}
		\caption{Test of overfitting in the $N_{Jet}\geq 6,~N_{tag}=3$ training.}
		\label{fig:Overtraining63}
	\end{minipage}
\end{figure}

Figures \ref{fig:Overtraining4} to \ref{fig:Overtraining63} show the corresponding distributions for the trainings relevant in this analysis. They are selected based on the expected discriminating power within the analysis categories (see section \ref{sec:8.1.4}).
In neither of them, the KS test yields values close to zero. Shapes training and testing samples are compatible with each other. Overtraining is not exposed for these trainings.


\Subsubsection{Selection of Observables}\label{sec:8.1.4}


\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/roc_matrix_edited.png}
	\caption{Overview on computed ROC integrals for all trainings on the x- and analysis categories on the y-axis. Trainings that are not considered due to overtraining are crossed.}
	\label{fig:ROCMatrix}
\end{figure}

All trainings are evaluated in all analysis categories. Those providing the highest integral of the receiving operator characteristic (ROC) curves are assumed to show the best separation of $t\bar{t}H(b\bar{b})$ to all its backgrounds. The integral values are summarized in matrix figure \ref{fig:ROCMatrix}.\newline 
Trainings for which overfitting is observed, are not considered. Chosen trainings for the statistical analysis are highlighted in red. Simulated classifier distributions in the analysis categories are shown separately for $t\bar{t}H(b\bar{b})$ and its backgrounds in figures \ref{fig:ObservableMC62} to \ref{fig:ObservableMC64}.

\begin{figure}[H]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/ge6j_eq2b__D_BDT_ttH_ge6j.pdf}
		\caption{Chosen observable in the 6j2b category. The discriminator has been trained with $N_{Jet}\geq6$ events.}
		\label{fig:ObservableMC62}
	\end{minipage}
	\hspace*{\fill}
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/eq4j_eq3b__D_BDT_ttH_eq4j.pdf}
		\caption{Chosen observable in the 4j3b category. The discriminator has been trained with $N_{Jet}=4$ events.}
		\label{fig:ObservableMC43}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/eq5j_eq3b__D_BDT_ttH_eq5j_eq3b.pdf}
		\caption{Chosen observable in the 5j3b category. The discriminator has been trained with $N_{Jet}=5,~N_{tag}=3$ events.}
		\label{fig:ObservableMC53}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/ge6j_eq3b__D_BDT_ttH_ge6j_eq3b.pdf}
		\caption{Chosen observable in the 6j3b category. The discriminator has been trained with $N_{Jet}\geq 6,~N_{tag}=3$ events.}
		\label{fig:ObservableMC63}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/eq4j_eq4b__D_BDT_ttH_eq4j.pdf}
		\caption{Chosen observable in the 4j4b category. The discriminator has been trained with $N_{Jet}=4$ events.}
		\label{fig:ObservableMC44}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/eq5j_ge4b__D_BDT_ttH_eq5j_eq3b.pdf}
		\caption{Chosen observable in the 5j4b category. The discriminator has been trained with $N_{Jet}=5,~N_{tag}= 3$ events.}
		\label{fig:ObservableMC54}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/8_Limit/8_1_SignalVsBackgroundSeparation/ge6j_ge4b__D_BDT_ttH_ge6j_eq3b.pdf}
		\caption{Chosen observable in the 6j4b category. The discriminator has been trained with $N_{Jet}\geq 6,~N_{tag}=3$ events.}
		\label{fig:ObservableMC64}
	\end{minipage}
	\hspace*{\fill}
	\begin{minipage}{0.45\textwidth}

	\end{minipage}
\end{figure}
\newpage