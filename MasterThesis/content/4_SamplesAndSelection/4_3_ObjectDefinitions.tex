\Subsection{Object Definitions}\label{sec:4.3}


\subsection*{Muons}
The following criteria are designed to select muons from decays of weak bosons. \newline
Muons are classified by their transverse momentum $p_t$, their pseudorapidity relative to the beam axis $\eta=-\ln{\left(\tan{\frac{\theta}{2}}\right)}$, their muon ID \cite{cms_muonID} and relative isolation ($relIso_{\mu}$) as defined in equation \ref{eq:muonIso}. There, the sums are evaluated over all particles reconstructed by the particle flow algorithm that lie within a cone $\Delta R=\sqrt{\left(\Delta \phi\right)^2+\left(\Delta \eta\right)^2}~< 0.4$ with respect to the tested muon. \textit{CH} labels charged hadrons originating from the primary vertex, \textit{NH} indexes neutral hadrons and \textit{PH} photons. \textit{PU} subscripts particles from other vertexes.
\begin{equation}
	relIso_{\mu}:~=~\frac{1}{p_t}\cdot \left( \sum_{i}^{}p_{t,i}^{CH}+\max\left(0,~ \sum_{i}^{}p_{t,i}^{NH} + \sum_{i}^{}p_{t,i}^{PH} - \frac{1}{2}\cdot \sum_{i}^{}p_{t,i}^{PU}\right) \right) ~.
\label{eq:muonIso}
\end{equation}
Muons with more than 5 GeV in $p_t$ that pass any muon ID undergo the selection procedure. Within the MiniAODv2 format in which recorded and simulated event information is stored, they are given in the \textit{slimmedMuons} collection \cite{cms_MiniAODWorkbook}. Signal muons for the analysis are required to have at least 25 GeV in $p_t$. They need to lie within $|\eta|~<$ 2.1, pass the tight muon ID and are well isolated quantified by $relIso_{\mu}<0.15$.\newline Furthermore, tight ID muons with at least 15 GeV in $p_t$, $|\eta|<2.4$ and $relIso_{\mu}~<$ 0.25 are labeled veto muons. Note that by definition, signal muons are also veto muons.


\subsection*{Electrons}
Similar to muons, the cuts for the electron selection are designed to select those from weak boson decays. Again, the classification is based on kinematic variables such as $p_t$ and $|\eta|$. Both a MVA based electron ID \cite{electrons_ID} and a relative isolation as written in equation \ref{eq:electronIso} are defined.
\begin{equation}
	relIso_e:~=~\frac{1}{p_t}\cdot\left( \sum_{i}^{}p_{t,i}^{CH}+\max\left(0,~ \sum_{i}^{}p_{t,i}^{NH} + \sum_{i}^{}p_{t,i}^{PH} - \rho\cdot A_{eff}  \right)\right) ~.
	\label{eq:electronIso}
\end{equation} 
The sums are evaluated over all particles reconstructed by the particle flow algorithm that lie within the cone $\Delta R=\sqrt{\left(\Delta \phi\right)^2+\left(\Delta \eta\right)^2}~< 0.3$ with respect to the tested electron. \textit{CH} labels charged hadrons originating from the primary vertex, \textit{NH} indexes neutral hadrons and \textit{PH} stands for photons. The parameter $\rho$ represents the energy density. $A_{eff}$ indicates an effective area such that its product with $\rho$ is designed to yield the average energy contribution due to pileup in the event \cite{electrons_effectiveArea}.
Electrons with more than 5 GeV in $p_t$ are stored within the \textit{slimmedElectrons} collection \cite{cms_MiniAODWorkbook} and undergo the selection procedure. Like for the muon definitions, signal electrons automatically fulfill the veto criteria, too. The presence of electrons is used to reject events such that only the veto definition needs to be checked. Veto electrons are required to have at least 15 GeV in $p_t$ and $|\eta|~<$ 2.4. They must fulfill the medium electron ID for Run 2 for which a kinematic dependent preselection has to be checked in advance \cite{electrons_ID}. Last, veto electrons are required to have $relIso_e<0.15$.


\subsection*{Jets}


In this analysis, jets are clustered with the anti-kt algorithm \cite{anti_kt} whose parameter $\Delta R$ is set to 0.4. In addition, charged hadrons from pileup interactions are rejected during this process \cite{cms_MiniAODWorkbook}. 
After a series of standard jet selection criteria based on energy fractions and quantities derived from its constituents \cite{cms_jetID}, jets whose axes are closer than $\Delta R~<$ 0.4 to any of the selected leptons above are discarded. The \textit{Fall15\textunderscore 25nsV2} jet energy corrections are applied thereafter to remove pileup energy contributions and the $p_t$-$\eta$ dependence on the jet energy response in the detector \cite{cms_JECPAS}. In data, an additional set of corrections is applied to correct for remaining differences between data and simulated jet responses ("L2L3Residual", see figure \ref{fig:L2L3Residual}) .
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/4_SamplesAndSelection/4_3_ObjectDefinition/L2L3Residual_cite_jecTwiki.png}
	\caption{L2L3Residual jet energy correction factors used in this analysis \cite{jecTwiki}.}
	\label{fig:L2L3Residual}
\end{figure}
The jet energy resolution in data and simulation are adjusted through a scaling procedure on simulated jets as described in equation \ref{eq:scalingJER}.
According scale factors \textit{SF}, matching of the reconstructed to generated jets and the random smearing recipe applied to certain jets are described in detail in \cite{cms_jerFactors}.
\begin{equation}
p_t~\rightarrow~\max\left(0.0, ~p_t^{gen}+SF\cdot\left(p_t-p_t^{gen}\right) \right) ~.
\label{eq:scalingJER}
\end{equation}
At the end, selected jets are required to have a transverse momentum of at least 30 GeV and $|\eta|~<$ 2.4. \newline
Jets are considered b-tagged if their combined secondary vertex discriminator \textit{pfCombinedInclusiveSecondaryVertexV2BJetTags} passes the medium working point.

