\Subsection{Monte Carlo Samples}\label{sec:4.2}

Signal events, i.e. $t\bar{t}H$($b\bar{b}$) whose semileptonic decay channel is illustrated in figure \ref{fig:ttHbbFeynman}, are generated with POWHEG Box Version 2 \cite{POWHEG} while the hadronization and fragmentation of quarks (parton showering) are simulated using PYTHIA8 \cite{PYTHIA8}. 
Background processes such as $t\bar{t}$ + Jets (fig. \ref{fig:ttJetsFeynman}) or electroweak top quark production (Single $t/\bar{t}$) (fig. \ref{fig:STFeynman}) tend to have similar signatures in the detector compared to the signal and need to be simulated in order to describe the recorded data in the relevant phase space. MG5AMG \cite{MadGraph} at next-to-leading order accuracy is deployed to generate most of these background samples.

 
\begin{figure}[H]
	\begin{minipage}{0.4\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{figures/4_SamplesAndSelection/4_1_MC/ttJets.png}
		\caption{Tree level Feynman diagram of the $t\bar{t}$ + Jets process in the semileptonic decay channel.}
		\label{fig:ttJetsFeynman}
	\end{minipage}
	\hfill
	\begin{minipage}{0.6\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/4_SamplesAndSelection/4_1_MC/ST_cite_topQuarkAtLHC.png}
		\caption{Single top quark production mechanisms at the LHC \cite{topQuarkAtLHC}.}
		\label{fig:STFeynman}
	\end{minipage}
\end{figure}


For each generated process, the final state particles are passed to a full simulation of the CMS detector implemented in GEANT4 \cite{GEANT4}. The steps afterwards, such as digitization and reconstruction of the objects, is performed with the CMSSW 76X framework. Table \ref{table:MCSamplesNominal} lists both the signal and the main background samples from the RunIIFall15- MiniAODv2-PU25nsData2015v1\textunderscore 76X\textunderscore mcRun2\textunderscore asymptotic\textunderscore v12-v1 campaign that are included in this analysis. The experimental conditions for the simulation are stored in the global tag 76X\textunderscore mcRun2\textunderscore asymptotic\textunderscore v12 \cite{cms_GTs}.
\begin{table}[H]
	\caption{List of the signal and main background samples in this analysis. Tables \ref{table:MCSamplesOthers} and \ref{table:MCSamplesStudies} complete this list.}
	\small
	\centering
	\begin{tabular}{llccc}
		\textbf{Process}&\textbf{Name} & $\boldsymbol{\sigma} $ \textbf{[pb]} & $\boldsymbol{N}$ & $\boldsymbol{N_{tr.}}$ \\
		\hline
		\midrule
		$t\bar{t}H$($b\bar{b}$) & ttHTobb\textunderscore M125\textunderscore 13TeV\textunderscore powheg\textunderscore pythia8  & 0.2935 & 3772012 & 548907\\
		\midrule 
		$t\bar{t}H$, other & \pbox{20cm}{ttHToNonbb\textunderscore M125\textunderscore 13TeV\textunderscore powheg\textunderscore pythia8} & 0.2112 & 3945824 & 817187\\
		\midrule 
		$t\bar{t}$ + Jets & \pbox{20cm}{TT\textunderscore TuneCUETP8M1\textunderscore 13TeV-powheg-pythia8}\tablefootnote{The extended samples ext3 and ext4 are used in this analysis.} & 831.76 & 197425642 & 42322439\\
		\midrule 
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore top\textunderscore 5f\textunderscore inclusiveDecays\textunderscore 13TeV-\\ powheg-pythia8\textunderscore TuneCUETP8M1} & 35.85 & 1000000 & 155401 \\
		\midrule 
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore antitop\textunderscore 5f\textunderscore inclusiveDecays\textunderscore 13TeV-\\powheg-pythia8\textunderscore TuneCUETP8M1} & 35.85 & 999400 & 155281\\
		\midrule
	\end{tabular}

	\label{table:MCSamplesNominal}
\end{table}

$\sigma$ represents the predicted Standard Model cross section at $\sqrt{s}$ = 13TeV in picobarn for each process \cite{xsec_ttH,xsec_ttJets,xsec_ST}. $N$ stands for the total available number of simulated events while the $N_{tr.}$ column shows the number of events passing the emulated IsoMu20 HLT path. \newline
Furthermore, dedicated simulations to cover the systematic uncertainty on the parton shower scale are provided for the $t\bar{t}$ + Jets and Single $t/\bar{t}$ processes. Table \ref{table:MCSamplesPSup} lists the samples for the upward variation and table \ref{table:MCSamplesPSdown} indicates those for the downward case of the parton showering. 

\begin{table}[H]
	\caption{Samples with upward variation of the parton showering scale.}
	\small
	\centering
	\begin{tabular}{lll}
		\textbf{Process}&\textbf{Name} & $\boldsymbol{N}$ \\
		\hline
		\midrule
		$t\bar{t}$ + Jets & \pbox{20cm}{TT\textunderscore TuneCUETP8M1\textunderscore13TeV \\-powheg-scaleup-pythia8} &  48427745 \\
		\midrule 
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore top\textunderscore 5f\textunderscore scaleup\textunderscore inclusiveDecays\textunderscore \\ 13TeV-powheg-pythia8\textunderscore TuneCUETP8M1} &  499200 \\
		\midrule
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore antitop\textunderscore 5f\textunderscore scaleup\textunderscore inclusiveDecays\textunderscore \\ 13TeV -powheg-pythia8\textunderscore TuneCUETP8M1} &  499600  \\
		\midrule
	\end{tabular}

	\label{table:MCSamplesPSup}
\end{table}

\begin{table}[H]
	\caption{Samples with downward variation of the parton showering scale.}
	\small
	\centering
	\begin{tabular}{lll}
		\textbf{Process}&\textbf{Name} &  $\boldsymbol{N}$ \\
		\hline
		\midrule
		$t\bar{t}$ + Jets & \pbox{20cm}{TT\textunderscore TuneCUETP8M1\textunderscore13TeV \\-powheg-scaledown-pythia8} & 49394023 \\
		\midrule 
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore top\textunderscore 5f\textunderscore scaledown\textunderscore inclusiveDecays\textunderscore \\ 13TeV-powheg-pythia8\textunderscore TuneCUETP8M1} &  500000 \\
		\midrule
		Single $t/\bar{t}$ & \pbox{20cm}{ST\textunderscore tW\textunderscore antitop\textunderscore 5f\textunderscore scaledown\textunderscore inclusiveDecays\textunderscore \\ 13TeV-powheg-pythia8\textunderscore TuneCUETP8M1}  & 495200 \\
		\midrule
	\end{tabular}
	\label{table:MCSamplesPSdown}
\end{table}

Finally, the $t\bar{t}$ + Jets sample is further divided into different classes according to the number of additional jet flavors besides those from the top quark decays. This is done in order to treat the uncertainties on cross section predictions separately of the individual subprocesses.
\begin{enumerate}
	\item $t\bar{t}+b\bar{b}$: at least two bottom-jets from one or more B-hadrons.
	\item $t\bar{t}+b$: exactly one bottom-jet from exactly one B-hadron.
	\item $t\bar{t}+2b$: exactly one bottom-jet from two or more B-hadrons.
	\item $t\bar{t}+c\bar{c}$: at least one charm-jet from one or more c-hadrons. 
	\item $t\bar{t}+lf$: none of the four above, no extra heavy flavor jet in this event.
\end{enumerate}	
The splitting is implemented within the CMS GenHFHadronMatcher tool and uses information on the generator level \cite{cms_GenHFHadronMatcher}.
Branching ratios that are obtained from its application on a subset of the $t\bar{t}$ + Jets sample are presented in table \ref{table:ttbarBR}.
\begin{table}[H]
	\caption{Branching ratios (BR) and their relative uncertainties ($\Delta$/BR) of the $t\bar{t}$ + Jets sample into the different classes. Details on the procedure are given in \cite{mt_heidemann}.}
	\centering
	\begin{tabular}{l|ccccc}
		$\boldsymbol{t\bar{t}~+~}$&$\boldsymbol{b\bar{b}}$ & $\boldsymbol{2b}$  & $\boldsymbol{b}$ & $\boldsymbol{c\bar{c}}$ & $\boldsymbol{lf}$ \\
		\hline
		\midrule
		\textbf{BR [\%]} & 0.521 & 0.533 & 1.877 & 9.39 & 87.67\\
		\midrule
		$\boldsymbol{\Delta}$/\textbf{BR [\%]} & 0.7 & 0.7 & 0.3 & 0.2 & 0.1\\
	\end{tabular}
	\label{table:ttbarBR}
\end{table}	