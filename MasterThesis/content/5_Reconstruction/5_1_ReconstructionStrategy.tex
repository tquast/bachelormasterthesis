\Subsection{Reconstruction Strategy}\label{sec:5.1}

A typical tree level $t\bar{t}H(b\bar{b})$ process as illustrated in figure \ref{fig:ttHbbFeynman} contains five composite objects:
\begin{itemize}
	\item $\boldsymbol{H}$: The Higgs boson decays into a pair of bottom quarks forming jets. The corresponding assignments are indexed $bH$ and $bbarH$. To avoid equivalent permutations through their commutativity, the convention is that $p_{t,bH}~>~p_{t,bbarH}$.
	\item $\boldsymbol{W_{had}}$: The W boson from one of the top quark decays. The $had$ subscript indicates its hadronic decay into two light flavored jets labelled $lj1$ and $lj2$. Similar to the Higgs decay products, indistinguishable combinations are reduced by demanding $p_{t,lj1}~>~p_{t,lj2}$.
	\item $\boldsymbol{W_{lep}}$: The W boson from the other top quark whose subscript $lep$ displays its decay into a lepton ($\mu$) and neutrino ($\nu_{\mu}$).
	\item $\boldsymbol{t_{had}}$: The top quark decays into $W_{had}$ and a bottom quark forming a jet labelled $bhad$.
	\item $\boldsymbol{t_{lep}}$: The top quark decays into $W_{lep}$ and a bottom quark forming a jet labelled $blep$.
\end{itemize}
The event selection as outlined in chapter \ref{sec:4.4} grants the presence of one muon and missing transverse energy in the detector. In the limit of vanishing neutrino mass, the W boson mass is fixed to 80.4 GeV to yield an analytic solution of the neutrino's four-momentum. In this context, its transverse momentum components are set to those of the missing transverse energy. References \cite{mt_rieger} and \cite{neutrinoReco} elucidate the details. 
Despite its simplicity, this approach performs similarily well in terms of reconstructed vs. true neutrino kinematics compared to other algorithms, e.g. \cite{alternativeNeutrinoReco} in which additional assumptions on the top quark mass and the $blep$ assignment are required.\newline\newline
The system configuration remains to be determined through the assignment of $N~\geq~4$ jets to the partons $bH$, $bbarH$, $lj1$, $lj2$, $bhad$ and $blep$.
In the following, one single set of assignments is referred to as a permutation.\newline\newline
Generator information is used to infer the \textit{true} permutation of an event. The generated partons' flight directions in the $\eta-\phi$ plane are known. Thus, a \textit{true} matching procedure associates the generated partons to the jets based on angular distances.\newline\newline
In real recorded data, generator information is not available. Therefore, the goal of the composite particle reconstruction algorithm is to find the permutation that fits best the physical knowledge on the $t\bar{t}H(b\bar{b})$ process. \newline In addition, the choice of composite particles for reconstruction is not trivial. Given a tree level signal process, jets formed by quarks from the hard interaction may not be selected at all, additional jets might be produced in initital or final state radiations or multiple quarks may be merged into a single jet. Therefore, given an event with a certain number of jets, it poses a hard constraint on the set of targeted composite objects. Here, the algorithm is applied for all possible combinations of composite objects in the $t\bar{t}H(b\bar{b})$ process simultaneously. Table \ref{table:recoHypotheses} defines the concept of hypotheses (h1-h11) introduced in this reconstruction. 

\begin{table}[H]
	\caption{Definition of the hypotheses, i.e. the assumed presence of composite particles in the event.}
	\centering
	\begin{tabular}{c|l|cccc}
		\textbf{N}$_{\boldsymbol{Jet}}$&\textbf{Hypothesis}& $\boldsymbol{t_{lep}}$ & $\boldsymbol{W_{had}}$ & $\boldsymbol{H}$ & $\boldsymbol{t_{had}}$ \\
		\midrule
		\multirow{8}{*}{\textbf{$\boldsymbol{\geq}$4}} & \textbf{h1} & \ding{51} &  &  &  \\
		& \textbf{h2} &  & \ding{51} &  &  \\
		& \textbf{h3} &  &  & \ding{51} &  \\
		& \textbf{h4} &  & \ding{51} &  & \ding{51} \\
		& \textbf{h5} & \ding{51} & \ding{51} &  &  \\
		& \textbf{h6} & \ding{51} &  & \ding{51} &  \\
		& \textbf{h7} &  & \ding{51} & \ding{51} &  \\
		& \textbf{h8} & \ding{51} & \ding{51} &  & \ding{51} \\
		\midrule
		\multirow{2}{*}{\textbf{$\boldsymbol{\geq}$5}}& \textbf{h9} & \ding{51} & \ding{51} & \ding{51} &  \\
		& \textbf{h10} &  & \ding{51} & \ding{51} & \ding{51} \\
		\midrule
		\multirow{1}{*}{\textbf{$\boldsymbol{\geq}$6}}& \textbf{h11} & \ding{51} & \ding{51} & \ding{51} & \ding{51} \\
	\end{tabular}
	\label{table:recoHypotheses}
\end{table}
For example, a $N_{Jet}=$4 event is reconstructed under eight different hypotheses, namely assuming the presence of only the $t_{lep}$ (h1) up to the assumption of finding $t_{lep}$, $t_{had}$ and $W_{had}$ (h8) among the selected jets. The list of possible hypotheses is extended for $N_{Jet}=$5 (h9, h10) and $N_{Jet}\geq$6 (h11) events. By design, the reconstruction of $t_{had}$ always includes $W_{had}$. 