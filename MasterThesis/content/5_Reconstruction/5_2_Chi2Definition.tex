\Subsection{$\chi^2$ Definition Extended by B-Tagging Information}\label{sec:5.2}%
%
For each permutation p under the assumption of hypothesis h to be covered by the selected jets in the event a score $\chi_{p,h}^2$ is assigned according to equation \ref{eq:chi2}. Reconstructed masses of composite objects in the hypothesis and combined secondary vertex (\textit{csv}) information are inputs to this formula.
\begin{equation}
	\begin{split}
		\chi_{p,h}^2~&:=~\sum_{k\in P_h}^{} \left(\frac{m_{p, k}-\hat{m}_k}{\sigma_{m_k}}\right)^2 \\
		&+~\Theta\left(H\in P_h\right) \cdot  \alpha_H \cdot \left(\left(1-csv_{p, bH}\right)^{\beta_H}+\left(1-csv_{p, bbarH} \right)^{\beta_H} \right)\\
		&+~\Theta\left(t_{had}\in P_h\right) \cdot  \alpha_{t_{had}} \cdot  \left(1-csv_{p, bhad}\right)^{\beta_{t_{had}}}\\
		&+~\Theta\left(t_{lep}\in P_h\right) \cdot  \alpha_{t_{lep}} \cdot \left(1-csv_{p,blep}\right)^{\beta_{t_{lep}}}\\
		&+~\Theta\left(W_{had}\in P_h\right) \cdot  \alpha_{W_{had}} \cdot \left( csv_{p, lj1}^{\beta_{W_{had}}}+csv_{p, lj2}^{\beta_{W_{had}}} \right)  ~.
	\end{split}
	\label{eq:chi2}
\end{equation}
 Permutations with close resemblance to the assumed hypothesis in terms of the involved quantities yield a low $\chi_{p,h}^2$. Consequently, minimizing $\chi_{p,h}^2$ gives the best fit and the respective permutation is likely to represent the system configuration under hypothesis h.\newline
In the following, the specific terms are defined:
\begin{itemize}
	\item $P_h$: The set of composite particles in the hypothesis h. For instance,\newline $P_{h11}~=~ \{H,~t_{had},~t_{lep},~W_{had}\}$.
	\item $m_{p,k}$: The reconstructed mass of composite particle k in the permutation p. These quantities are calculated for each permutation.
	\item $\hat{m}_k,~\sigma_{m_k}$: Mass parameters deduced from the true matching of the jets to the generated partons in the $t\bar{t}H(b\bar{b})$ process. They constitute the mass distributions at 100\% reconstruction efficiency of that composite object. Their inference is shown in figure \ref{fig:massPlots}.
\end{itemize}
Up to this point, the first sum represents a kinematic criterion to evaluate the goodness of permutations. It ensures that those permutations with reconstructed masses far beyond their predictions are disfavoured. Experimental smearing effects are incorporated into the expectation through the aforementioned fits to the mass peaks.\newline
The next summands consider the branching ratios of W bosons and top quarks into bottom quarks \cite{pdg}:
\begin{align*}
	|V_{tb}|^2~\approx~1  ~,\\
	|V_{ub}|^2~+~|V_{cb}|^2~=~0.00355^2~+~0.0414^2~\approx~0 ~.
\end{align*}   


\begin{figure}[H]
	\includegraphics[width=0.45\textwidth]{figures/5_Reconstruction/5_2_Chi2Definition/m_H.png}
	\hfill
	\includegraphics[width=0.45\textwidth]{figures/5_Reconstruction/5_2_Chi2Definition/m_t_had.png}
	\includegraphics[width=0.45\textwidth]{figures/5_Reconstruction/5_2_Chi2Definition/m_t_lep.png}
	\hfill
	\includegraphics[width=0.45\textwidth]{figures/5_Reconstruction/5_2_Chi2Definition/m_W_had.png}
	\caption{Reconstructed mass distributions of $H$ (top left), $t_{had}$ (top right), $t_{had}$ (bottom left) and $W_{had}$ (bottom right). Generator information are employed as outlined in order to find the true configuration. Gaussian fits in the displayed range are applied to determine the means $\overline{m_{k}}$ and the widths $\sigma_{m_k}$. The full signal $t\bar{t}H(b\bar{b})$ sample after the nominal event selection is used.}
	\label{fig:massPlots}
\end{figure}

Based on them, top quarks are assumed to always decay into a bottom quark and W boson while the latter presumably never produces b-flavored jets. These simplifications justify the following terms:
\begin{itemize}
	\item $\Theta\left(X \in P_h\right)$: The $\Theta$ function compiles to one if composite particle X is in $P_h$ and else zero.
	\item $\left(1-csv_{p, bH} \right),~\left(1-csv_{p, bbarH} \right)$: In the signal process, the true jets from the Higgs decay are b-flavored. Hence, \textit{csv} values close to zero are punished while those closer to one are favoured by not enhancing the $\chi_{p,h}^2$ significantly.
	\item $\left(1-csv_{p, bhad} \right),~\left(1-csv_{p, blep} \right)$: Similar reasoning as for $bH$ and $bbarH$. Top quarks decay into bottom quarks resulting in b-flavored jets with \textit{csv} values tendentially closer to one.
	\item $csv_{p, lj1},~csv_{p, lj2}$: W bosons almost never produce bottom quarks. The produced jets are most likely to be light flavored such that \textit{csv} values closer to zero are favoured.
\end{itemize} 
$\alpha_Y$ and $\beta_Y$ are tuning parameter because the degree of punishment of incongruous \textit{csv} values in the slot of $\textit{Y}$ is not trivial. Their values are estimated by testing many settings and determining which one results in the highest efficiency when reconstrucing the full $t\bar{t}H(b\bar{b})$ process (=h11) on a subset of $N_{Jet}~\geq~6$ events.
During the scan, the $\alpha_Y$ and $\beta_Y$ have been summarized to one $\alpha$ and $\beta$ in order to reduce necessary computation resources: 
\begin{equation*}
	\begin{split}
		\alpha~&:=~\alpha_{H}=\alpha_{t_{had}}=\alpha_{t_{lep}}=\alpha_{W_{had}}  ~,\\ 
		\beta~&:=~\beta_{H}=\beta_{t_{had}}=\beta_{t_{lep}}=\beta_{W_{had}}  ~.\\
	\end{split}
\end{equation*}

It is found that $\alpha~=~6.0$ and $\beta~=1.0$ performs best when scanning $\alpha~\in~\{0.0,0.5,1.0,...,7.0\}$ and $\beta~\in~\{0.5,1.0,1.5,2.0\}$. The full event reconstruction efficiency based on 3561 fully reconstructed $t\bar{t}H(b\bar{b})$ events by the generator is shown in figure \ref{fig:betaInfluence} for floating $\alpha$ at a fixed $\beta~=~1$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/5_Reconstruction/5_2_Chi2Definition/alpha_influence.png}
	\caption{h11 reconstruction efficiency on 3561 fully reconstructed $t\bar{t}H(b\bar{b})$ events by the true matching depending on $\alpha$ at a fixed $\beta~=~1$.}
	\label{fig:betaInfluence}
\end{figure}

