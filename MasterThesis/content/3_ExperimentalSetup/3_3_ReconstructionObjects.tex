\Subsection{Reconstruction of Physics Objects}\label{sec:3.3}

Recorded signals by the detector represent a high degree of abstraction and their translation into physics objects is necessary. Tracker readout and energy deposits in the calorimeters provide the necessary information.
For this purpose, CMS has adopted the Particle Flow algorithm, which is outlined first. Afterwards, special emphasis is put on the most relevant objects in this search. Those are muons, particle jets, in particular b-tagged jets, and missing transverse energy in events. 


\subsubsection*{Particle Flow Algorithm}

The Particle Flow (PF) algorithm aims at the reconstruction of all stable particles produced in the collision of two protons. 
Information from all subdetectors are considered in this procedure to improve the overall resolution and efficiency.
The starting point is provided by the tracker in which tracks (blue lines in figure \ref{fig:ParticleFlow}) are formed from hits in the tracker layers. Subsequently, vertices are reconstructed using an adaptive fitting method \cite{vertexFitting}. Tracks are assigned softly to the primary vertices and also matched to energy clusters (grey bars in fig. \ref{fig:ParticleFlow}). Those are defined as accumulations of energy deposits inside the calorimeter. Linking of tracks to energy clusters is done by a tailored algorithm that also respects information from the muon chambers. These associations ultimately form particle hypotheses that may be accepted or rejected when tighter criteria are applied. Identification of particles considers the location of energy deposits (red dots in fig. \ref{fig:ParticleFlow}). For instance, a charged pion is not associated to a track that does not point to an energy cluster in the HCAL.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.50\textwidth]{figures/3_ExperimentalSetup/3_3_ReconstructionObjects/ParticleFlow_cite_cmsPF.png}
	\caption{Illustration of PF, track to cluster assignment at CMS. Circles mark the inner boundaries of the E- and HCAL \cite{cmsPF}. X and Y are the coordinates in the transverse plane.}
	\label{fig:ParticleFlow}
\end{figure}

\subsubsection*{Muons}\label{sec:3.3.2}
Muon reconstruction at CMS starts with two different track definitions. \textit{Tracker} tracks are those constructed from tracker information only while \textit{standalone-muon} tracks incorporate signals from muon chambers only. Based on that, three alternative methods are followed \cite{muon_reco}. First \textit{global muons} are obtained from matching the outer standalone-muon tracks to inner tracker tracks. Both are combined to one track using Kalman-filter fitting. This procedure improves the momentum resolution significantly for muons with transverse momentum ()$p_t$) greater than 200 GeV. 
\newline Secondly, \textit{tracker muons} are the result from an inside-out approach. Under consideration of the magnetic field, energy losses and all other detector smearing effects tracker tracks with $p_t>$0.5 GeV and $|\vec{p}|>$2.5 GeV are extrapolated to the muon chamber region here. If the extended track can be matched to at least one active segment in the muon chambers, it is assigned to a tracker muon.
\newline Last, \textit{PF muon} reconstruction incorporates the identical conditions to the two mentioned before. In addition, futher selection criteria based on subsystem information are applied. These cuts are not static, but for example depend on its kinematics, and are designed to optimize the efficiency for muons within jets.
 
\subsubsection*{Jets}\label{sec:3.3.3}
Outgoing quarks from hard interaction processes are never observed directly due to hadronization and fragmentation processes. Instead, they form numerous stable hadrons and other secondary particles (figure \ref{fig:Jets}) that are identified by PF. 
Jet clustering combines these hadrons and clusters them into so-called jets. It can be seen as an attempt of reverse-engineering the effects of hadronization and fragmentation. CMS has adopted the anti-k$_t$ algorithm. It guarantees that the definition of jets is not sensitive to soft radiation of gluons or underlying underground processes in the event. The algorithm gives perfectly conical hard jets.
A measure of distance $d_{ij}$, between particle entities $i$ and $j$, being either particles or pseudojets, is introduced as in equations \ref{antiKtmeasure} \cite{jetsDetector}.
\begin{equation}
	\begin{split}
		d_{ij}&=\min{\left(p_{t,i}^{-2},p_{t,j}^{-2}\right)}\cdot\frac{\Delta_{i,j}^2}{R^2}~, \\
		\Delta_{ij}^2&=\left(y_i~-~y_j \right)^2~+~\left(\phi_i~-~\phi_j \right)^2 ~.
	\end{split}
\label{antiKtmeasure}
\end{equation}
In each step, $d_{ij}$ is calculated for all pairs of entities. Afterwards, the smallest distance $d_{min}$ is identified. If $d_{min}$ is also smaller than every $p_{ti,}^{-2}$, the two objects making $d_{min}$ are clustered and form a new entity by adding their four-vectors. In case the minimal $p_{t,i}^{-2}$ is smaller than $d_{min}$, entity $i$ is called a jet and not considered any further during the clustering. The procedure is iterated until all entities have been treated. The parameter R represents a measure for the cone of the resulting jets. CMS has chosen $R=0.4$ for their standard jet definition. 

\subsubsection*{B-Tagging}\label{sec:3.3.4}
Bottom quarks predominantly hadronize into B-hadrons. Jets including B-hadron decays are usually referred to as b- or bottom flavored jets. \newline
They tend to differ from others due to the hard fragmentation of the bottom quark to such hadrons, their large mass and leptonic content because B-hadrons decay semileptonically in 40\% of all cases \cite{bTagExercise}. Most significantly, the hadrons' long lifetime at the order of picoseconds, results in secondary vertices that are measurably displaced from the primary ones by a few millimeters (see figure \ref{fig:bTagPrinciple}). The combined secondary vertex algorithm (\textit{csv}) considers related information to identify such jets. Quantities derived from reconstructed secondary vertices, impact parameters and decay lengths are input to an artifical neural network whose output value ranges from zero to one yielding a discriminator to each jet being rather b-flavored ($\rightarrow$ 1) or not ($\rightarrow$ 0). It is the most effective \cite{btag_csvEffectiveness} tagging algorithm at CMS and is currently used in its second version. In this analysis, its medium working point at 0.8 is chosen. On the recorded 2015 dataset, it corresponds to more than 67.8\% tagging efficiency for jets with $p_t>30$ GeV while mistagging occurs with 1\% probability \cite{cms_btagPerformance}.

\begin{minipage}{0.45\textwidth}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{figures/3_ExperimentalSetup/3_3_ReconstructionObjects/jets_cite_jetsDetector.png}
		\caption{Illustration of particle jets in the detector \cite{jetsDetector}.}
		\label{fig:Jets}
	\end{figure}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{figures/3_ExperimentalSetup/3_3_ReconstructionObjects/btag_cite_bTagExercise.png}
		\caption{Sketch of the relevant characteristics of jets including B-hadron decays \cite{bTagExercise}.}
		\label{fig:bTagPrinciple}
	\end{figure}
\end{minipage}

\subsubsection*{Missing Transverse Energy}\label{sec:3.3.5}
Despite constructed essentially as a hermetic detector, the vectorial sum of reconstructed particles' transverse momenta does not add up to zero. Missing transverse energy (MET) is defined in forming the transverse momentum-vector sum over all reconstructed particles in the event and then taking the opposite of this azimuthal, momentum two-vector (equation \ref{eq:MET}) \cite{cmsPF}.
\begin{equation}
	\cancel{\vec{E}}_T=-\sum_{i=1}^{\text{PF particles}}\vec{p}_{t,i}=:\vec{p}_{t,\nu}~.
	\label{eq:MET}
\end{equation}
This momentum imbalance is attributed to undetectable particles. Within the Standard Model, neutrinos are the only candidates while theories beyond allow for other particles as well.
In this thesis, MET is fully interpreted as the transverse components of the neutrino from the semileptonically decaying $t\bar{t}$ system. 

