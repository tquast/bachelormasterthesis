\Subsection{Event Yields and Control Plots}\label{sec:7.2}%
%
Predicted event rates are directly influenced by the presented corrections. Tables \ref{table:yieldsControlAndBkg} and \ref{table:yieldsMixedAndSignal} present the expected yield of signal and background events in each analysis category for an integrated luminosity of $\mathcal{L}_{int}=2.24$ fb$^{-1}$. Actual recorded yields are shown in the "Data" column. Samples are grouped into physically motivated labels as listed in tables \ref{table:MCSamplesNominal} and \ref{table:MCSamplesOthers}. The major background is constituted by top quark pairs and additional jets. Note that the $t\bar{t}H$ process in which the Higgs boson does not decay into a pair of bottom quarks is considered a background in the analysis. Stated uncertainties account for the limited number of available simulated events (Monte Carlo statistics). 
The categories containing exactly four jets tend to have more recorded data than predicted by the simulation. Conversely, the $N_{Jet}\geq6$ categories are influenced by underprediction of yields. This fact is likely to be attributed to an unfavorable choice of the strong coupling constant $\alpha_s$ in the showering model. Otherwise, systematic uncertainties on the assumed cross section (e.g. $\Delta \sigma_{t\bar{t}+Jets}/\sigma_{t\bar{t}+Jets}=10\%$) may account for deviances in the data per background yield comparison. \newline

The number of expected signal per backgrounds events increases with the jet and b-tag multiplicities. The ratio is growing up to 0.033 in the $\geq$6j $\geq$4b category and underlines the naming convention in figure \ref{fig:Categories}.\newline
Modelling for a selection of five basic event observables is validated in the 4j2b and 5j2b control regions in figures \ref{fig:control_muEta} to \ref{fig:control_METPt}. Error bars visualize the uncertainties on the Monte Carlo statistics only. In agreement with table \ref{table:yieldsControlAndBkg} the overall normalization in the 4j2b category appears to be underestimated in the simulation. Besides that, the shape of the kinematic variables (figures \ref{fig:control_muEta} - \ref{fig:control_METPt}) of the selected muon, the jet with highest $p_t$ and the missing transverse energy indicate a good level of agreement. Despite the applied csv reweighting, the combined secondary vertex value of the jet with highest $p_t$ in the event indicates deviances up to 20\% in the 5j2b category. It is argued that the deviations fluctuate around the baseline and the disagreement shows no linear trend. Therefore, this slight incompatibility is assumed to be negligible. \newline
In summary, the background modelling is considered on a reliable level for the analysis.
\begin{table}[H]
	\caption{Predicted and recorded event yields in the control and background analysis regions after the reweightings discussed in section \ref{sec:7.1}.}
	\centering
	\begin{tabular}{c|cccc}
		& \textbf{4j 2b} & \textbf{5j 2b} & \textbf{$\boldsymbol{\geq}$6j 2b} & \textbf{4j 3b}\\
		\midrule
		\midrule
		$\boldsymbol{t\bar{t}H}(\boldsymbol{b\bar{b}})$ & \textbf{5.94}$ \boldsymbol{\pm}$\textbf{0.05}  & \textbf{6.29}$ \boldsymbol{\pm}$\textbf{0.05} & \textbf{7.29}$ \boldsymbol{\pm}$\textbf{0.06}& \textbf{3.25}$ \boldsymbol{\pm}$\textbf{0.03}\\
		\midrule
		$t\bar{t}H$, other & $ 3.32\pm $0.03 & 4.33$ \pm $0.04 & 6.77$ \pm $0.05 & 0.37$ \pm $0.01\\
		$t\bar{t} + lf$ & 12477$ \pm $13 & 5260$ \pm $8 & 2535$ \pm $6 & 1015$ \pm $4\\
		$t\bar{t} + cc$ & 1325$ \pm $4 & 990$ \pm $4 & 813$ \pm $3& 176$ \pm $2 \\
		$t\bar{t} + 2b$ & 102$ \pm $1 & 84$ \pm $1 & 78$ \pm $1 & 38.6$ \pm $0.7\\
		$t\bar{t} + b$ & 340$ \pm $2 & 245$ \pm$2 & 185$ \pm $2 & 100$ \pm $1\\
		$t\bar{t} + bb$ & 80$ \pm $1 & 84$ \pm $1 & 106$ \pm $1 & 37.3$ \pm $0.7\\
		Single $t/\bar{t}$ & 471$ \pm $9 & 215$ \pm $6& 101$ \pm $4& 37$ \pm $2 \\
		V + Jets & 340$ \pm $10 & 141$ \pm $6 & 65$ \pm $3 & 19$ \pm $2\\
		VV + Jets & 16$ \pm $3 & 3$ \pm $1 & 0.8$ \pm $0.6 & 2$ \pm $1\\
		$t\bar{t}$V + Jets & 49.7$ \pm $0.7 & 54.4$ \pm $0.7 & 90.0$ \pm $0.9 & 6.0$ \pm $0.2\\
		\midrule
		\textbf{Total Bkg.} & \textbf{15204}$ \boldsymbol{\pm}$\textbf{20} & \textbf{7081}$ \boldsymbol{\pm}$\textbf{13} & \textbf{3971}$ \boldsymbol{\pm}$\textbf{9}& \textbf{1432}$ \boldsymbol{\pm}$\textbf{6} \\
		\midrule
		\textbf{Data} & \textbf{15848}$ \boldsymbol{\pm}$\textbf{126} & \textbf{7168}$ \boldsymbol{\pm}$\textbf{85} & \textbf{3558}$ \boldsymbol{\pm}$\textbf{60}& \textbf{1387}$ \boldsymbol{\pm}$\textbf{37} \\
		\midrule
		\textbf{S/B} & $\boldsymbol{<}$\textbf{0.001} & \textbf{0.001} & \textbf{0.002} & \textbf{0.002}\\
	\end{tabular}
	\label{table:yieldsControlAndBkg}
\end{table}

\begin{table}[H]
	\caption{Predicted and recorded event yields in the mixed and signal analysis regions after the reweightings discussed in section \ref{sec:7.1}}
	\centering
	\begin{tabular}{c|ccccc}
		& \textbf{5j 3b} & \textbf{$\boldsymbol{\geq}$6j 3b} & \textbf{4j 4b}& \textbf{5j $\boldsymbol{\geq}$4b} & \textbf{$\boldsymbol{\geq}$6j $\boldsymbol{\geq}$4b}\\
		\midrule
		\midrule
		$\boldsymbol{t\bar{t}H}(\boldsymbol{b\bar{b}})$ & \textbf{4.70}$ \boldsymbol{\pm}$\textbf{0.04} & \textbf{6.63}$ \boldsymbol{\pm}$\textbf{0.05} & \textbf{0.45}$ \boldsymbol{\pm}$\textbf{0.01} & \textbf{1.31}$ \boldsymbol{\pm}$\textbf{0.02}  & \textbf{2.91}$ \boldsymbol{\pm}$\textbf{0.03} \\
		\midrule
		$t\bar{t}H$, other & 0.68$ \pm $0.01 & 1.54$ \pm $0.02 & 0.02$ \pm $0.00 & 0.05$ \pm $0.00 & 0.21$ \pm $0.01  \\
		$t\bar{t} + lf$ & 506$ \pm $3 & 290$ \pm $2 & 10.0$ \pm $0.4 & 9.48$ \pm $0.38 & 9.8$ \pm $0.4  \\
		$t\bar{t} + cc$ & 180$ \pm $2 & 193$ \pm $2 & 6.0$ \pm $0.3 & 10.3$ \pm $0.4 & 18.6$ \pm $0.5  \\
		$t\bar{t} + 2b$ & 42.4$ \pm $0.8 & 47.7$ \pm $0.8 & 1.8$ \pm $0.2 & 3.7$ \pm $0.2 & 6.4$ \pm $0.3  \\
		$t\bar{t} + b$ & 96$ \pm $1 & 84$ \pm$1 & 3.8$ \pm $0.2 & 6.8$ \pm $0.3 & 10.2$ \pm $0.4  \\
		$t\bar{t} + bb$ & 55.9$ \pm $0.9 & 89$ \pm $1 & 5.0$ \pm $0.3 & 14.9$ \pm $0.4 & 36.3$ \pm $0.7  \\
		Single $t/\bar{t}$ & 27$ \pm $2 & 17$ \pm $2 & 0.4$ \pm $0.2 & 1.5$ \pm $0.5 & 1.4$ \pm $0.4  \\
		V + Jets & 9$ \pm $1 & 6.0$ \pm $0.6 & 0.11$ \pm $0.04 & 0.8$ \pm $0.3 & 0.5$ \pm $0.2  \\
		VV + Jets & 0.3$ \pm $0.2 & 0.2$ \pm $0.2 & 0.00$ \pm $0.00& 0.3$ \pm $0.3 & 0.00$ \pm $0.00  \\
		$t\bar{t}$V + Jets & 9.8$ \pm $0.3 & 20.1$ \pm $0.4 & 0.37$ \pm $0.06 & 1.23$ \pm $0.09 & 3.83$ \pm $0.16  \\
		\midrule
		\textbf{Total Bkg.} & \textbf{927}$ \boldsymbol{\pm}$\textbf{4} & \textbf{749}$ \boldsymbol{\pm}$\textbf{4} & \textbf{27.6}$ \boldsymbol{\pm}$\textbf{0.7}& \textbf{49}$ \boldsymbol{\pm}$\textbf{1} & \textbf{87}$ \boldsymbol{\pm}$\textbf{1}  \\
		\midrule
		\textbf{Data} & \textbf{909}$ \boldsymbol{\pm}$\textbf{30} & \textbf{693}$ \boldsymbol{\pm}$\textbf{26} & \textbf{45}$ \boldsymbol{\pm}$\textbf{7} & \textbf{58}$ \boldsymbol{\pm}$\textbf{8} & \textbf{75}$ \boldsymbol{\pm}$\textbf{9}  \\
		\midrule
		\textbf{S/B} & \textbf{0.005} & \textbf{0.009} & \textbf{0.016}& \textbf{0.027}& \textbf{0.033} \\
	\end{tabular}
	\label{table:yieldsMixedAndSignal}
\end{table}


\begin{figure}[H]
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq4j_eq2b__mu_eta.pdf}
	\hfill
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq5j_eq2b__mu_eta.pdf}
	\caption{Pseudorapidity of the selected muon in the 4j2b (left) and 5j2b (right) categories.}
	\label{fig:control_muEta}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq4j_eq2b__jet1_pt.pdf}
	\hfill
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq5j_eq2b__jet1_pt.pdf}
	\caption{Highest transverse momentum among the selected jets in the 4j2b (left) and 5j2b (right) categories.}
	\label{fig:control_jet1Pt}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq4j_eq2b__met_pt.pdf}
	\hfill
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq5j_eq2b__met_pt.pdf}
	\caption{Missing transverse energy in the 4j2b (left) and 5j2b (right) categories.}
	\label{fig:control_METPt}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq4j_eq2b__jet1_csv.pdf}
	\hfill
	\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_2_EventYields/eq5j_eq2b__jet1_csv.pdf}
	\caption{Combined secondary vertex of the leading jet in the 4j2b (left) and 5j2b (right) categories.}
	\label{fig:control_jet1CSV}
\end{figure}
