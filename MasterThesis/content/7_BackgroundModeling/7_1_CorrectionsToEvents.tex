\Subsection{Corrections to Simulated Events}\label{sec:7.1}
In general, simulations are meant to model observable distributions of real physics data. However, exact agreement is not always achieved by the event generation, showering and detector simulations only. This circumstance is treated by the introduction of weight factors which are attributed to the events. Discrepancies both in total event yields and in shapes of certain observables are compensated by this means. 
In the following, four types of corrections that are applied on simulated events are explained.

\subsubsection{Luminosity Normalization}
In principle, the amount of simulated events available to an analysis is limited by computation resources. Naturally, this choice does not affect their frequency in real data. The latter depends solely on the cross section $\sigma_p$ of the particular process $p$ and the integrated luminosity $\mathcal{L}_{int}$ delivered by the experiment.
\begin{align*}
	N_{true}~=~\sigma \cdot \mathcal{L}_{int} ~.
\end{align*}  
So, if $N$ events are generated, each one of them obtains a weight $\omega_{\mathcal{L}_{int}}$ in order to reproduce the expectancy in yields (equation \ref{eq:lumiweight}).
\begin{equation}
	\omega_{\mathcal{L}_{int}}~=~\frac{\sigma_p\cdot \mathcal{L}_{int}}{N} ~.
	\label{eq:lumiweight}
\end{equation}

\subsubsection{Muon Scale Factors}
The probability to successfully select an event that contains one muon fulfilling the defined criteria is given by the efficiency $\epsilon$. It can be factorized into contributions from single steps of the muon selection. In this channel, muons events are selected by the dedicated high level trigger, their assigned muon ID and relative isolation (see section \ref{sec:4.3}). Thus, the overall efficiency can be written in form of equation \ref{eq:muonEff} \cite{muonSFs}.
\begin{equation}
	\epsilon~=~\epsilon_{ID} \cdot \epsilon_{Iso} \cdot \epsilon_{Trigger}  ~.
	\label{eq:muonEff}
\end{equation}

Efficiencies are measured both in simulation and in data. For recommended working points, the official CMS Muon Physics Objects Group computes the values. Tag and probe methods are implemented for the according measurements in real data, in which the amount of correct and false selections is inferred from so called standard candles like the $\mu\mu$ invariant mass from Z boson or $J/\Psi$ decays \cite{cms_MuonTagAndProbe}.
Scale factors account for discrepancies between the efficiencies in simulation and in data. Simulated events are assigned a set of weights $SF_{ID/Iso/Trigger}$ to match the efficiency in Monte Carlo to the measured one in data (equation \ref{eq:SFMuon}).
\begin{equation}
	SF_{ID/Iso/Trigger}~=~\frac{\epsilon_{ID/Iso/Trigger}^{Data}}{\epsilon_{ID/Iso/Trigger}^{MC}} ~.
	\label{eq:SFMuon}
\end{equation}
Figure \ref{fig:muonTriggerSFs} shows exemplary the used single muon trigger scale factors. They were measured on the 2015 RunC and RunD datasets with an integrated luminosity of 2.27 fb$^{-1}$ and a leading order $Z\rightarrow l\bar{l}$ Monte Carlo dataset. Scale factors are provided in bins of $p_t$ and $|\eta|$ of the selected muon.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{figures/7_BackgroundModeling/7_1_CorrectionsToEvents/mutrigger_weights_cite_muonSFs.png}
	\caption{Muon trigger scale factors provided by the POG \cite{muonSFs}.}
	\label{fig:muonTriggerSFs}
\end{figure}

\newpage
\subsubsection{Pileup Scenario Corrections}
Due to LHC's high instantaneous limininosity, multiple inelastic interactions between the colliding protons occur at each bunch crossing. This so called in-time pileup results in the creation of numerous primary vertices inside the bunch dimensions. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{figures/7_BackgroundModeling/7_1_CorrectionsToEvents/pu_weights_cite_puWeight_puCalc.png}
	\caption{Pileup scenario in simulation and the 2015 RunD dataset. Weights in black are computed as the quotient of both histograms. N$_{\text{PV}}$ represents the number of primary vertices.}
	\label{fig:PUWeights}
\end{figure}
By contrast, out-of-time pileup is caused by the small spacing between bunches that may collide outside the designated interaction area but the products still reach the inside of the detector. In both cases, pileup deposits additional energy in the detector, influences its readout and reconstruction efficiencies. Hence, the pileup scenario in simulation has to be adjusted to the true situation in data.\newline
Figure \ref{fig:PUWeights} shows the assumed pileup scenario in simulations and the recorded one in data. Both curves are normalized to the same integral value. The number of primary vertices which is equivalent to the number of inelastic interactions in the event is plotted on the x-axis. The data scenario is calculated using the CMS pileup calculation tool \cite{puCalc} assuming an inelastic proton-proton cross section of $\sigma_{pp}=69.0$ mb. Weights in black are derived as a quotient of both curves \cite{puWeight} and are applied to the simulated events.

\subsubsection{B-Tag Reweighting}
The distibution of the combined secondary vertex ($csv$) value for heavy and light flavored jets are calibrated in order to grant a sufficiently good modelling of the data. \newline
\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{figures/7_BackgroundModeling/7_1_CorrectionsToEvents/csv_weights_cite_csvFiles.png}
	\caption{Exemplary light flavor scale factor \cite{csvFiles} as a function of $csv$ in fixed bin of $p_t$ and $|\eta|$ used in this analysis.}
	\label{fig:csvDistribution}
\end{figure}

Since this analysis strongly relies on b-tagging in terms of categorization of events and input variables for the final classifier (see section \ref{sec:8.1.2}), the compensation of any discrepancies is crucial. For the $t\bar{t}H(b\bar{b})$ search at CMS, a tag-and-probe technique is adopted to derive scale factors for every jet \cite{btagSFProcedure}. 
The true flavors of simulated jets are known. They are derived independently for heavy ($HF$) and light flavored ($LF$) jets. For this purpose, cuts to restrict the phase space to a heavy (light) flavored enriched region are performed.  More precisely, in a given bin of $p_t$, $|\eta|$ and $csv$ the scale factors are computed by the prescription in equation \ref{eq:SFBtag}. 
\begin{equation}
	SF_{LF(HF)}\left(csv,p_t,\eta\right)~=~\frac{Data-MC_{LF(HF)}}{MC_{HF(LF)}} ~.
\label{eq:SFBtag}
\end{equation}
The quotient of data yield minus the predicted light (heavy) contamination per yield of simulated heavy (light) flavor jets defines the scale factors. These scale factors are again centrally provided and applied on simulation only. Final event weights are computed as the product (equation \ref{eq:SFBtagWeight}) of the scale factors all selected jets.
\begin{equation}
	\omega_{Event,csv}~=~\prod_{i=1}^{N_{Jets}}SF_i	 ~.
	\label{eq:SFBtagWeight}
\end{equation}


