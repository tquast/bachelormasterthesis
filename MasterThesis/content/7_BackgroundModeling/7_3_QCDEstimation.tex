\Subsection{Estimation of QCD Multijet Contributions}\label{sec:7.3}%
QCD multijet production refers to a variety of well understood and thus well known Standard Model processes. For instance, at hadron colliders, the fusion of quark and antiquarks to an intermediate gluon forming two or more jets through initial- and final-state radiation constitutes a typical multijet process \cite{QCDMultijet_feynman}. 
Usually, the predicted QCD multijet cross section exceeds those of other considered processes by many orders of magnitudes. Hence, the high-level trigger, object identifications and the offline event selection (sections \ref{sec:4.3} and \ref{sec:4.4}) are designed to restrict the final state phase space to fully exclude multijet events from the analysis categories. \newline
As a sanity check, the simulation of roughly 17 million QCD events, for which the scalar sum of the reconstructed jets' transverse momenta lies between 300 and 500 GeV (table \ref{table:MCSamplesStudies}), is run analyzed. For this specific sample, the predicted cross section is above 347 nb \cite{xsec_QCD}, approximately 400 times higher than for the main $t\bar{t}$+Jets background.
In total, three events are not filtered by the cuts. Extrapolating to $\mathcal{L}_{int}=2.24$ fb$^{-1}$ results in an expected number of 138 events in the phase space of interest. Assuming standard poisson errors, the assigned relative uncertainty on this number is 58\%. It illustrates that simulation of many more multijet events are required for a reliable prediciton of their yield in the phase space of interest. Since computing resources are limited, either the absence of such events is assumed arguing that the cuts are sufficiently tight or data driven methods can be used to infer their contribution in the signal- from sideband regions. \newline
In this search, it is believed that multijet event contributions are overall negligible. This hypothesis is strengthened by the procedure described in the following. For that purpose, the multijet yield is inferred from fits of QCD templates to data in sideband regions. Derivations of observable shapes like in \cite{dt_klingebiel} are not aimed at in this approach.\newline Section \ref{sec:7.3.1} explains the factorization based method and emphasizes its limitations. The technical template fitting procedure to obtain QCD yields in the sideband regions is subject to \ref{sec:7.3.2}. Results and their implications to the analysis are described in \ref{sec:7.3.3}.


\Subsubsection{Application of the Factorization Method}\label{sec:7.3.1}

\begin{wrapfigure}{r}{0.4\textwidth}
		\includegraphics[width=0.4\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/MuonSelectionInverted.png}
		\caption{}
		\label{fig:InvertedMuon}
\end{wrapfigure}
An analysis that relies on sequential cuts in two variables may profit from a data-driven background estimation using the factorization method. Two variables are chosen such that the removal of related cuts significantly enhances the yield of background events in question. In this search, the cuts on the number of b-tagged jets in the event and on the relative isolation of signal muons are modified for this purpose. The minimum number of b-tagged jets is lowered to one while the allowed relative isolation of signal muons is extened to 1.0. To be precise, the latter inversion is not strictly sequential as it interferes with the veto on additional muons in the event. Figure \ref{fig:InvertedMuon} illustrates the scenario in which the inverted muon selection rejects events that would pass the nominal criteria. Blue color shades the allowed phase space in $p_t$ (x-axis) and relative isolation (y-axis) for signal muons in the inverted selection. Green represents the nominal case for signal and red for veto muons. An event with two muons at the positions marked by {\fontfamily{lmr}\selectfont$\star$} passes the nominal selection since, by the according definitions, it contains one signal muon and no vetos. Within the inverted recipe, two signal muons are identified and the event is discarded. The inverted selection has been applied to an exemplary simulated dataset. Among 2051 events with more than one signal muon selected with the inverted criteria, the situation as in figure \ref{fig:InvertedMuon} applies to 128. For that sample, 15409 events pass the nominal signal muon selection. Consequently, less than one percent of nominally passing events are discarded by the inverted selection. This minor impact is assumed to not influence the estimation. \newline \newline
Subsequently, the usual analysis categories are subdivided into four regions labelled A-D. Figure \ref{fig:QCDCategories} depicts the applied cuts for those. Note that regions B and D are shared for all categories with identical jet multiplicity.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/Categorization.png}
	\caption{Sketch of regions A-D. The x-axis shows the relative muon isolation and the number of b-tagged jets is plotted on the y-axis. Region A: Nominal analysis region with usual number of b-tagged jets ($=2$, $=3$, $\geq 4$) and relative muon isolation of the signal muon below 0.15. Region B: Exactly one b-tagged jet and relative muon isolation of the signal muon below 0.15. Region C: Usual number of b-tagged jets and relative muon isolation of the signal muon above 0.4. Region D: Exactly one b-tagged jet and relative muon isolation of the signal muon above 0.4.}
	\label{fig:QCDCategories}
\end{figure}
The yield of background events in question $N_A$ in analysis region A can be inferred from yields in the sideband regions B-D if the following two criteria are met:
\begin{enumerate}
	\item The number of b-tagged jets in the event and the signal muon's relative isolation must be uncorrelated \cite{DataDrivenBkgEstimation}. Figuratively, it implies that the shape of their distributions do not depend on cuts on the other.
	\item Sideband regions B, C, D must be dominated by multijet events \cite{cms_QCDInWTauNu}.
\end{enumerate}
Then, the factorization method applies and the ABCD-formula according to equation \ref{eq:ABCD} can be used.

\begin{equation}
	\frac{N_A}{N_B}=\frac{N_C}{N_D}~\Rightarrow~N_A=N_B \cdot \frac{N_C}{N_D} ~.
	\label{eq:ABCD}
\end{equation}
The inverted selection is applied to all background samples in the analysis, the single muon dataset and leptonically enriched $p_t$-binned QCD samples as listed in table \ref{table:MCSamplesStudies}. Figure \ref{fig:QCDCorrelation5} shows the two dimensional distribution of the number of b-tagged jets and the selected muon's relative isolation for data events with exactly five selected jets. The correlation coefficient is determined to $\rho=0.09$. It is considered to fulfill the first criterion for the application of equation \ref{eq:ABCD}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.65\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/correlations_data_nJet=5.png}
	\caption{Correlation between the inverted variables in the $N_{Jet}=5$ bin. See appendix \ref{sec:appendix_QCD} for $N_{Jet}=4(\geq 6)$.}
	\label{fig:QCDCorrelation5}
\end{figure}

However, figure \ref{fig:mtW52_B} reveals the domination of background events in sideband region B for $N_{Jet}=5$. The second prerequisite for the ABCD-formula is not necessarily fulfilled. At this point, it is emphasized again that this method is not aimed at a fully reliable prediction of $N_A$ in each analysis category. Rather, the obtained order of magnitude in the yields are considered trustworthy to strengthen the initial hypotheses of full multijet abscence in the relevant phase space.


\Subsubsection{Template Fitting}\label{sec:7.3.2}
In this section, the technical implementation of the inference of $N_{B,C,D}$ is explained. As discussed before, the obtained yields through simulations are connected with large relative errors. 

\begin{figure}[H]
		\centering
		\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/mtW_5j2b.png}
	\hfill
		\centering
		\includegraphics[width=0.49\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/MET_5j2b.png}
		\caption{Shapes of the leptonically decaying W boson's transverse mass (left) and the missing transverse energy (right) for the 5j2b category. Note that the green and red shapes are shared for all $N_{Jet}=5$ categories.}
		\label{fig:QCDObservables52}
\end{figure}

Therefore, a template fit to fundamental observables is applied. It aims at the reproduction of these observables' shapes as recorded in data through the variation of the multijet normalizations in question. 
Uncertainties due to the limited amount of data, of the background and of the QCD predictions are incorporated through the repetition of this procedure with varied templates. 
The transverse mass of the leptonically decaying W boson and the MET in the event are chosen. 
 Their observed, normalized distributions in data are shown in figure \ref{fig:QCDObservables52} for the $N_{Jet}=5$ case. MET is correlated to the designated region A-D. Namely, the inversion of the muon's relative isolation criterion, leads to the inclusion of less boosted W boson decays in the sample. Thus, neutrinos are less energetic and the measured MET is lowered. In contrast, $m_{T, Wlep}$ maintains its shape. Ideally, the fits to both observables should not deviate with respect to their errors.\newline \newline
 In the following, the general steps are described and illustrated for the 5j2b control region.\newline
 First, a likelihood for the simulation to represent the data in terms of the respective observable is defined according to equation \ref{eq:QCDLikelihood}. A poisson distribution of the data yield $d_i$ with expectation value $\mu_i=b_i +\mu_{QCD} \cdot s_i$ is considered for each of the $N$ bins. $\mu_{QCD}$ symbolizes a global fit parameter by which the simulated yield of multijet events is scaled to best fit the data. In oppostion to fitting fractions of templates like with ROOT's TFractionFitter class \cite{ROOT}, the predicted background shape and normalization is fixed.
 \begin{equation}
	 L~=~L\left(data~|\mu_{QCD}, bkg, QCD  \right)~=~\prod_{i=1}^{N} \frac{e^{-b_i-\mu_{QCD}\cdot s_i}\cdot\left(b_i+ \mu_{QCD}\cdot s_i\right)^{N_i}}{N_i !} ~.
	 \label{eq:QCDLikelihood}
 \end{equation}
 \newpage
On the left of figure \ref{fig:mtW52_C} the templates for the transverse W boson mass in region C of the 5j2b category is illustrated. 
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/QCD_fits_nJet=5_nTag=2_C_mtW.png}
	\caption{Fitting procedure for the 5j2b category in region C. Left: Nominal $m_{T, Wlep}$ templates. Middle: Negative, logarithmic likelihood for the unshifted case. Right: Distribution of $N_{QCD}$ among thousand iterations.}
	\label{fig:mtW52_C}
\end{figure}
Maximization of the likelihood function is equivalent to minimizing its negative logarithm that is computed in the middle. $\mu_{QCD}$ is plotted on the x-axis. The vertical line indicates its global minimum as obtained by using ROOT's implementation of Brent's method \cite{BrentsMethod}. 
\begin{equation}
	 L_{min}~=~L\left(data~|\mu_{QCD}^{best},bkg, QCD  \right) ~.
\end{equation}
In this particular case, $\mu_{QCD}=1.0$ does not correspond to the best fit which one would naively expect from simulation only. Equation \ref{eq:YieldQCDIteration} states the conversion to a number of QCD events in this region obtained by the fit:
\begin{equation}
	N_{QCD}~=~\mu_{QCD}^{best}\cdot \sum_{i=1}^{N} s_i ~.
	\label{eq:YieldQCDIteration}
\end{equation}
The righthand graph displays the distribution of $N_{QCD}$. For this purpose, the outlined fit method is iterated 1000 times. For each iteration, the entries in the histogram $x_i$ ($x\in \{s,b,d\})$ are obtained through shifting the nominal ones $x_i^0$ by their statistical uncertainty $\sigma_{x_i^0}$ (equation \ref{eq:binVariation}). One may interpret this method as a repeated conduction of the observable's measurement. Here, true values are naively set to the unshifted ones.
\begin{equation}
	x_i = \max{\left(x_i^0+\sigma_{x_i^0}\cdot r,~0\right)},~r= \text{Gaussian random number with } \mu=0 \text{, }\sigma=1 ~.
	\label{eq:binVariation}
\end{equation}
The entire procedure is performed for regions B, C, D both for $m_{T, Wlep}$ and MET. Figures \ref{fig:mtW52_B} and \ref{fig:mtW52_D} show the result for the B and for the D region respectively using $m_{T, Wlep}$.
Gaussians are fitted to the $N_{QCD}$ distributions. The means are taken as the best guesses for $N_{B,C,D}$. Widths are interpreted as their spreads, i.e. the measure by how far a single sample of $N_{B,C,D}$ statistically deviates from its mean value. 

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/QCD_fits_nJet=5_nTag=2_B_mtW.png}
	\caption{Fitting procedure for the 5j2b category in region B. Left: Nominal $m_{T, Wlep}$ templates. Note the dominance of the contribution by background processes. Middle: Negative, logarithmic likelihood for the unshifted case. Right: Distribution of $N_{QCD}$ among thousand iterations.}
	\label{fig:mtW52_B}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/7_BackgroundModeling/7_3_QCDEstimation/QCD_fits_nJet=5_nTag=2_D_mtW.png}
	\caption{Fitting procedure for the 5j2b category in region D. Left: Nominal $m_{T, Wlep}$ templates. Middle: Negative, logarithmic likelihood for the unshifted case. Right: Distribution of $N_{QCD}$ among thousand iterations.}
	\label{fig:mtW52_D}
\end{figure}

Tables \ref{table:QCD42} and \ref{table:QCD52} summarize the finally computed yields of multijet events in the sideband regions and the proper propagation to the region of interest for the analysis control categories.

\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 4j2b. See also \ref{fig:mtW42_B}-\ref{fig:mtW42_D}.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{4j 2b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B} & 2266$\pm$342 &2036$\pm$202\\ 
			\textbf{C} & 1871$\pm$63 &1892$\pm$59\\ 
			\textbf{D} & 5164$\pm$122 &5158$\pm$735\\ 
			\midrule
			\textbf{QCD} & 821$\pm$128 &747$\pm$132\\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{5.5\%}$\boldsymbol{\pm}$\textbf{0.9\%} &  \\
		\end{tabular}
		\label{table:QCD42}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 5j2b. Graphs are shown in the text above.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{5j 2b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 313$\pm$91 & 335$\pm$79 \\ 
			\textbf{C}& 462$\pm$33 & 453$\pm$38 \\ 
			\textbf{D}& 1078$\pm$47 & 1048$\pm$52 \\ 
			\midrule
			\textbf{QCD}& 134$\pm$41 & 145$\pm$37 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.} & & \textbf{2.1\%}$\boldsymbol{\pm}$\textbf{0.5\%} \\
		\end{tabular}
		\label{table:QCD52}
	\end{table}	
\end{minipage}  

\vspace{10pt}

For both of them, the fraction of multijet events per background yield is at the percent level and not compatible with zero. Within their errors, all yields obtained from MET and $m_{t,W}$ are in agreement with each other. Since these categories constitute control regions, thus they are not included in the actual analysis, the estimated multijet contribution is not incorporated there.

\Subsubsection{Estimated Yields in Analysis Categories}\label{sec:7.3.3}
Multijet contributions are estimated for all analysis categories that are used in the measurement. Results are given in tabular form and are commented. Implications to the analysis are explained.

\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 4j3b category. See also fig. \ref{fig:mtW43_C}.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{4j 3b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 2266$\pm$342 & 2036$\pm$202 \\ 
			\textbf{C}& 39$\pm$13 & 40$\pm$14 \\ 
			\textbf{D}& 5164$\pm$122 & 5158$\pm$735 \\ 
			\midrule
			\textbf{QCD}& 17$\pm$6 & 16$\pm$6 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{1.2\%}$\boldsymbol{\pm}$\textbf{0.4\%} & \\
		\end{tabular}
		\label{table:QCD43}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
Complete absence of QCD events is not in agreement with the result. It is assumed that this particular background constributes similarly like other backgrounds to the final observable. Thus, an additional, uncorrelated to all other systematics, conservative 2\% uncertainty on the backrgound rate is introduced to account for this contribution. 
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 4j4b category. See also fig. \ref{fig:mtW44_C}.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{4j 4b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 2266$\pm$342 & 2036$\pm$202 \\ 
			\textbf{C}& 13$\pm$9 & 12$\pm$10 \\ 
			\textbf{D}& 5164$\pm$122 & 5158$\pm$735 \\ 
			\midrule
			\textbf{QCD}& 6$\pm$4 & 5$\pm$4 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{23.2\%}$\boldsymbol{\pm}$\textbf{15.5\%} & \\
		\end{tabular}
		\label{table:QCD44}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
	The yield of QCD events in the relevant phase space is estimated to 23.2\% relative to all other backgrounds. Yet, the hypothesis of no multijet events in the analysis region is compatible within the uncertainty of this method. No action is undertaken.
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 5j3b category. See also fig. \ref{fig:mtW53_C}.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{5j 3b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 313$\pm$91 & 335$\pm$79 \\ 
			\textbf{C}& 32$\pm$11 & 34$\pm$10 \\ 
			\textbf{D}& 1078$\pm$47 & 1048$\pm$52 \\ 
			\midrule
			\textbf{QCD}& 9$\pm$4 & 11$\pm$4 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& & \textbf{1.2\%}$\boldsymbol{\pm}$\textbf{0.4\%} \\
		\end{tabular}
		\label{table:QCD53}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
The identical reasoning as for the 4j3b category applies here. Thus, an additional, uncorrelated to all other systematics, 2\% uncertainty on the backrgound rate is introduced.
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 5j4b category. See also fig. \ref{fig:mtW54_C}.}
		\centering
		\begin{tabular}{c|cc}
			\textbf{5j} $\boldsymbol{\geq}$ \textbf{4b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 313$\pm$91 & 335$\pm$79 \\ 
			\textbf{C}& 3$\pm$2 & 3$\pm$2 \\ 
			\textbf{D}& 1078$\pm$47 & 1048$\pm$52 \\ 
			\midrule
			\textbf{QCD}& 0.8$\pm$0.6 & 0.9$\pm$0.7 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& & \textbf{2.1\%}$\boldsymbol{\pm}$\textbf{1.6\%} \\
		\end{tabular}
		\label{table:QCD54}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
QCD events in the relevant phase space make up an additional 2.1\% with respect to all other backgrounds. 
But, the hypothesis of no multijet events in the analysis region is compatible within the uncertainty of this method. No action is undertaken.	
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 6j2b category. See also \ref{fig:mtW62_B}-\ref{fig:mtW62_D}.}
		\centering
		\begin{tabular}{c|cc}
			$\boldsymbol{\geq}$\textbf{6j} \textbf{2b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 7$\pm$0 & 4.6$\pm$0.2 \\ 
			\textbf{C}& 155$\pm$20 & 162$\pm$20 \\ 
			\textbf{D}& 216$\pm$27 & 322$\pm$26 \\ 
			\midrule
			\textbf{QCD}& 5.0$\pm$0.9 & 2.3$\pm$0.4 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{0.1\%}$\boldsymbol{\pm}$\textbf{0.0\%} & \\
		\end{tabular}
		\label{table:QCD62}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
The estimated yield of QCD events by this method is negligible with respect to the rate uncertainties on the other background processes. An inclusion is not necessary.	
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 6j3b category. See also \ref{fig:mtW63_C}.}
		\centering
		\begin{tabular}{c|cc}
			$\boldsymbol{\geq}$\textbf{6j} \textbf{3b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\ 
			\hline
			\midrule
			\textbf{B}& 7$\pm$0 & 4.6$\pm$0.2 \\ 
			\textbf{C}& 5$\pm$1 & 5$\pm$1 \\ 
			\textbf{D}& 216$\pm$27 & 322$\pm$26 \\ 
			\midrule
			\textbf{QCD}& 0.16$\pm$0.05 & 0.07$\pm$0.02 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{$<$0.1\%}$\boldsymbol{\pm}$\textbf{0.0\%} & \\
		\end{tabular}
		\label{table:QCD63}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
The estimated yield of QCD events by this method is negligible with respect to the rate uncertainties on the other background processes. An inclusion is not necessary.		
\end{minipage}


\begin{minipage}{0.45\textwidth}
	\begin{table}[H]
		\caption{QCD yields in 6j4b category. See also \ref{fig:mtW64_C}.}
		\centering
		\begin{tabular}{c|cc}
			$\boldsymbol{\geq}$\textbf{6j} $\boldsymbol{\geq}$ \textbf{4b}& $\boldsymbol{m_{t, Wlep}}$& $\boldsymbol{MET}$\\
			\hline 
			\midrule
			\textbf{B}& 7$\pm$0 & 4.6$\pm$0.2 \\ 
			\textbf{C}& 1$\pm$2 & 1$\pm$2 \\ 
			\textbf{D}& 216$\pm$27 & 322$\pm$26 \\ 
			\midrule
			\textbf{QCD}& 0.03$\pm$0.09 & 0.01$\pm$0.03 \\
			\midrule\rowcolor{gray!25}
			\textbf{QCD / bkg.}& \textbf{$<$0.1\%}$\boldsymbol{\pm}$\textbf{0.0\%}  & \\
		\end{tabular}
		\label{table:QCD64}
	\end{table}	
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
The estimated yield of QCD events by this method is negligible with respect to the rate uncertainties on the other background processes. An inclusion is not necessary.		
\end{minipage}