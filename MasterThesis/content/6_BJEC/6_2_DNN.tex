\Subsection{Deep Neural Networks}\label{sec:6.2}%
%
In the following, the core principles of deep neural networks are briefly explained. Instead of reviewing the concepts in a general frame, the focus is set on the concrete application in this analysis. A step-by-step introduction to and a broad overview of this topic may be found in \cite{DNN_book,natureNN}.\newline

Neural networks are a programming paradigm that enables machines to learn classification or regression tasks through training of exemplary data. Explicit engineering from outside by humans is not necessary. The idea is to feed data in a raw form as inputs to the designed network and to expect a certain output, for example a prediction of some number. Figure \ref{fig:NNPrinciple} depicts the architecture of a typical feed-forward network. If the targeted quantity is somehow encoded in the inputs, the network will automatically detect those correlations through mathematical transformation of the data from one level to an other. As a matter of fact, any mathematical function can be learnt in the limit of infinite network sizes \cite{natureNN}. In this way, abstract representations are formed. Due to the increasing abstraction in these layers, they are often referred to as hidden units or hidden layers.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/6_BJEC/6_2_DNN/NN_idea_cite_natureNN.png}
	\caption{Sketch of a feed-forward neural network with three in- and two outputs \cite{natureNN}. The components in vectors $\vec{y}_{Hi}$ are referred to as nodes.}
	\label{fig:NNPrinciple}
\end{figure}
For the prediction of correction factors in the context of this regression, a feed-forward network is chosen. $N$ input features are defined for each b-tagged jet. Those are arranged as an $N$-dimensional input vector $\vec{x}$ and subsequently fed into the input layer of the network $f$. $t$, the correction factor in question, is its output. Mathematically, the prescription can be written as shown in equation \ref{eq:networkPrescription}.
\begin{equation}
	f:~\mathbb{R}^N\rightarrow \mathbb{R},~\vec{x}\rightarrow t ~.
	\label{eq:networkPrescription}
\end{equation}

The transformation $T$ from one $N_1$-dimensional hidden layer $H1$ to the next $H2$ with dimension $N_2$ can be written as in equation \ref{eq:layerTrafo}.
\begin{equation}
	\begin{split}
		T:&~\mathbb{R}^{N_1}\rightarrow\mathbb{R}^{N_2},~\vec{y}_{H1}\rightarrow \vec{y}_{H2}  ~,\\
		\vec{y}_{H2}=&~\vec{u}\left(M_{12}\cdot \vec{y}_{H1}~+~\vec{b}_{12}\right) ~.
	\end{split}
	\label{eq:layerTrafo}
\end{equation}
$M_{12}~\in ~ \mathbb{R}^{N_2xN_1}$ is the so called weight matrix that contains internal net parameters. Its non-zero entries are visualized by the connecting arrows in figure \ref{fig:NNPrinciple}. The term biases refers to an additional set of internal parameters and is represented by $\vec{b}_{12}\in \mathbb{R}^{N_2}$. These biases govern the amount of activation in the hidden layers. A highly negative bias usually implies the corresponding node to be inactive. Last, $\vec{u}$ is a vector-valued function in which the same scalar function $u$ is applied on each component as written in equation \ref{eq:uFunction}. 
\begin{equation}
	\begin{split}
		\vec{u}:~&\mathbb{R}^N\rightarrow \mathbb{R}^N,~\vec{x}\rightarrow 	\vec{y} ~,\\
		y_i~=~&u\left(x_i\right) ~.
	\end{split}
	\label{eq:uFunction}
\end{equation}

Typical choices for $u$ are the sigmoid function $u(x)=\frac{1}{1+e^{-x}}$, the exponential- $u(x)=\Theta\left(-x\right)\cdot \left(e^x-1\right)+\Theta\left(x\right)\cdot x$ or the rectified linear unit $u(x)=\max\left(x,~0 \right)$.
The non-linearity of $u(x)$ exhibits the learning power of the network. If all layers were connected by linear functions, the last layer would be lineary dependent on the input with the result that hidden layers would not learn the desired abstract representations. 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/6_BJEC/6_2_DNN/functions.png}
	\caption{Comparison of common activation functions $u(x)$.}
\end{figure}

Deep learning is the generic term for a set of techniques to train a neural network to predict the correct outcome. Training refers to the stepwise adjustment of internal net parameters in the transfer matrixes $M_{ij}$ and bias vectors $\vec{b}_{ij}$. 
Here, the concept of supervised learning is used. Samples of size $K$ are evaluated by the network. Afterwards, a measure of error in the predicted outcome $t_i$ with respect to the true value $\hat{t_{i}}$ is computed. The sum of quadratic deviation as defined in equation \ref{eq:cost} is employed in this regression.
\begin{equation}
	C~:=~ \sum_{i=1}^{K} \left( t_i~-~\hat{t}_i \right)^2 ~.
	\label{eq:cost}
\end{equation}
$C$ is called the \textit{cost} function and depends on all internal parameters. At each iteration, the high dimensional gradient is computed and internal parameters are updated to minimize $C$. The technical implementation, makes use of backpropagation, also known as the reverse mode of automatic differentation \cite{backpropagation}. It calculates long chained derivatives efficiently and thus allows for training of nets with many hidden layers. \newline
Overfitting, i.e. the memorization of specific representations that are only given in the training sample, always needs to be checked on a statistically independent set of inputs. In general, there are multiple procedures to avoid overfitting during the learning. This regression applies dropout during the training. Randomly, single connections are removed temporarily with a probability $\left(1-p_{keep}\right)$ for each minimizing step. Therefore, dropout prevents the network from focussing on single representations for their final evaluation and does not build up strong connections through high weights between particular nodes. An equivalent picture is to understand dropout as the simultaneous training of an ensemble of many networks.
