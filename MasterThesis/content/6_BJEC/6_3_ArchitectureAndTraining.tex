\Subsection{Network Architecture and Training}\label{sec:6.3}%
%
 
Tensorflow \cite{tensorflow} is used for the implementation and training of the network. It is an open source library providing powerful algorithms for numerical computation using data flow graphs . Due its broad applicability to a variety of domains, it is well suited for conducting deep neural network research. \newline 
The designed network consists of three hidden layers made up by 50 nodes each. Rectified linear units are consistently chosen as transfer functions. All layers are connected with each other granting the transfer of learned representations at each layer to all the following. Weights and biases are initialized with Gaussian distributed numbers with mean 0.01 and standard deviation 0.001. 76 different variables are used for the regression. The inputs defined per jet are motivated in a regression performed in another $H\rightarrow b\bar{b}$ analysis \cite{bJEC_CMS}. High correlations among some of them are not expected to restrain the performance of the network. Badly modeled variables in the simulations such as charge related quantities are excluded. The set of input variables is listed below and grouped to the context in which they are defined.
\vspace{2mm}
\newline
\textbf{\underline{Variables defined per jet (see also \cite{patJetsManual})}}
\begin{itemize}
	\item Jet kinematics (10): energy, transverse energy, momentum, transverse momentum, momentum components, invariant mass, flight direction in $\phi$ and $\eta$.
	\item Applied jet energy correction factor (see section \ref{sec:4.3}).
	\item Constituent based quantities identified by particle flow (5): number of constituents in the jet, distribution of their transverse momenta, their spread in $\eta - \phi$ and the second transverse energy weighted moments in $\eta - \eta$ and $\eta - \phi$ space.
	\item Multiplicities of particle classes among the constituents (7): muons, electrons, neutral hadrons, neutral particles, photons, charged hadrons, charged particles.
	\item Area of the jets. It is the extent of the region in $\eta-\phi$ space occupied by a very large number of infinitely soft particles that are artificially added in the event and clustered into the jet to change its properties \cite{jetArea}.
	\item Vertex quantities (7): 3D impact parameter significance and value, vertex mass, number of tracks emerging from it, momentum components. 
	\item Combined secondary vertex value.
	\item Information of closest lepton within $\Delta R<0.3$ to the jet axis (3): angular distance, transverse momentum, relative transverse momentum to the jet. These values are set to -1 but still included if no lepton satisfies the angular distance criterion.
\end{itemize}

\textbf{\underline{Variables defined per event}}
\begin{itemize}
	\item Energy density $\rho$ in the event.
	\item Object multiplicities (4): number of jets and leptons in the event.
	\item Kinematic sums of objects (16): the sums of momentum components and energies of jets, muons, electrons and taus.
	\item Sum of angular distances between objects of the same type (8): sums of $\Delta \phi$ and $\Delta \eta$ for all pairs of jets, electrons, muons and tau leptons.
	\item Sum of angular distances between objects of different type (12): sums of $\Delta \phi$ and $\Delta \eta$ for all pairs of jets-electrons, jets-muons, .., muons-tau leptons.
\end{itemize}
Intentionally, the per event variables are incorporated as inputs to provide a handle to the true missing transverse energy in the event. Invisible neutrino contributions in the jets are added up into this quantity. Reversely, providing handcrafted representations for it might result in another abstract correlation to the targeted contributions that the network might be able to learn during the training. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\textwidth]{figures/6_BJEC/6_3_Architecture_Training/performance.png}
	\caption{Convergence of the cost function in the training. $N_{epoch}$ symbolizes the number of iterations through the training set.}
	\label{fig:NNCost}
\end{figure}
Approximately five million b-tagged jets are used for the network's training. Dropout is chosen such that hidden connections are randomly removed with $1-p_{keep}~=~0.2$ probability. The set is traversed 500 times at sample sizes of 100.000 events per minimizing step. \textit{Adam} is a stochastic optimization method in which the learning rate, i.e. the amount by which internal parameters are modified, is adaptive \cite{AdamOptimizer}. It is used for the minimization. The initial learning rate is set to $10^{-5}$. 
Figure \ref{fig:NNCost} illustrates the convergence of $C$ (equation \ref{eq:cost}) with increasing number of iterations through the dataset.\newline
The evaluation on jets both in the analysis and the test samples uses tfdeploy \cite{tfdeploy}. It is a python library providing a lightweight interface to store, read and evaluate tensorflow graphs.
The performance of the deep neural network is illustrated in figure \ref{fig:NNTargetsVsRegressed}. One million b-tagged jets independent from the training sample have been evaluated with the trained network. This two-dimensional histogram shows the predicted regression factor $t$ on the x-axis against the true correction factor $\hat{t}$ on the y-axis. Ideally, the majority of entries lies on the black angle bisector implying a good agreement of evaluated to true correction factors by the network. Overall, the majority of evaluated cases are found underneath the bisector. This fact indicates an overcorrection adopted by the network. Jet energies are tendentially corrected by factors larger than truly necessary to obtain the aimed transverse momentum. The impacts of the applied regression on reconstructed mass resolutions and reconstruction efficiencies are studied in the next chapter.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.50\textwidth]{figures/6_BJEC/6_3_Architecture_Training/target_vs_regressed.png}
	\caption{Predicted vs. targeted output of the trained network. Ideally, all entries would be filled close on the bisecting black line.}
	\label{fig:NNTargetsVsRegressed}
\end{figure}