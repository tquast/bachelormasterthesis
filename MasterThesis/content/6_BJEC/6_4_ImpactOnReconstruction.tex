\Subsection{Impact on Event Reconstruction}\label{sec:6.4}%
%

The correspondence of the reconstructed jets' transverse momenta to those of their assigned jet on the generator level are shown in figure \ref{fig:NNPtDistribution}. 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/6_BJEC/6_4_ImpactOnReconstruction/pt_vs_ptgen_edited.png}
	\caption{$p_t / p_{t,gen}$ distribution of one million b-tagged jets before (blue) and after (red) the applied energy correction by the regression network. Gaussian fits in the ranges indicated by the solid lines are performed to quantitatively characterize the peaks.}
	\label{fig:NNPtDistribution}
\end{figure}
One million simulated b-tagged jets from the semileptonically enriched $t\bar{t}$+Jets sample are evaluated by the network and corrected accordingly. 
In agreement with figure \ref{fig:regTarget}, the blue histogram is shifted towards a mean below one and shows a clear tail on the lefthand side. Furthermore, overcorrection by the regression is illustrated. Namely. the red curve shows the transverse momentum ratios after the applied regression. Its peak is clearly shifted to a mean above one. Nevertheless, the intended improvement in $p_t$ resolution is achieved. Both discussed graphs are created with unapplied jet energy resolution factors. The true distributions of transverse momentum ratios of simulated b-tagged jets in the analysis are equally smeared. Therefore, the statement of improved jet energy resolution still holds.
\begin{figure}[H]
	\includegraphics[width=0.49\textwidth]{figures/6_BJEC/6_4_ImpactOnReconstruction/mH.png}
\hfill
	\includegraphics[width=0.49\textwidth]{figures/6_BJEC/6_4_ImpactOnReconstruction/mthad.png}
	\includegraphics[width=0.49\textwidth]{figures/6_BJEC/6_4_ImpactOnReconstruction/mtlep.png}
\hfill
	\includegraphics[width=0.49\textwidth]{figures/6_BJEC/6_4_ImpactOnReconstruction/mWhad.png}
	\caption{Reconstructed mass distributions of $H$ (top left), $t_{had}$ (top right), $t_{had}$ (bottom left) and $W_{had}$ (bottom right) with (red) and without (black) applied regression. The full signal $t\bar{t}H(b\bar{b})$ sample after the nominal event selection is used.}
	\label{fig:best_masses_regressed}
\end{figure}
The additional correction is applied in the analysis prior to the event reconstruction. For this purpose, the factors like in equation \ref{eq:target} are multiplied to the four-momenta of selected and b-tagged jets. The impact on reconstructed masses of composite objects in the $t\bar{t}H(b\bar{b})$ process is shown in figure \ref{fig:best_masses_regressed}.
Resolutions are determined using Gaussian fits in the displayed ranges. The absolute change in widths $\sigma$ and relative change in resolutions $\frac{\sigma}{m}$ are summarized in table \ref{table:massResolutions}.
\begin{table}[H]
	\caption{Summary of Gaussian fits to the mass distributions in figure \ref{fig:best_masses_regressed}. Masses $m$ and widths $\sigma$ are given in GeV.}
	\small
	\centering
	\begin{tabular}{c|ccc|ccc|c}
		&$\boldsymbol{m_{nom}}$ &  & $\boldsymbol{m_{reg}}$ & $\boldsymbol{\sigma_{m,nom}}$ &  & $\boldsymbol{\sigma_{m, reg}}$ & $\boldsymbol{1- \frac{\sigma_{m,reg}\cdot m_{nom}}{\sigma_{m,nom}\cdot m_{reg}}}$ [\%]\tablefootnote{A conservative estimation to subtract the effect of overcorrection by 3\% is to scale the quotient by 1.03. The according numbers for the $H$ ($t_{had/lep}$) are -4.4 (-2.7).}\\
		\hline
		\midrule
		\textbf{H} & 114.5 & $\rightarrow$ & 119.8 & 17.0 & $\rightarrow$ & 16.5 & \textbf{-7.2} \\
		$\boldsymbol{t_{had}}$ & 170.1 & $\rightarrow$ & 173.2  & 20.5 & $\rightarrow$ & 19.7 & \textbf{-5.6} \\
		$\boldsymbol{t_{lep}}$ & 169.9 & $\rightarrow$ & 173.3 & 26.8 & $\rightarrow$ & 25.8 & \textbf{-5.6} \\
		$\boldsymbol{W_{had}}$ & 81.7 & $\rightarrow$ & 81.9 & 11.3 & $\rightarrow$ & 11.2 & \textbf{-1.1} \\	
	\end{tabular}
	\label{table:massResolutions}
\end{table}	
Composite objects decaying into bottom flavored jets profit from the additional correction in terms of narrower invariant mass distributions. For instance, the two bottom flavored jets from the Higgs boson decay are likely to be b-tagged. Likewise, both of them undergo the additional energy correction procedure. As a result, the two jets from which the Higgs boson is reconstructed show an improved agreement to their true value which explains the sharper $m_H$ distribution after the regression. By contrast, the reconstructed $W_{had}$ mass distribution whose produced jets are unlikely to be corrected by this procedure since they tend to be identified as light flavored jets does not profit significantly from it. \newline\newline
Narrower mass peaks provide a stronger constraint in the event reconstruction in this thesis. The $\chi^2$ (equation \ref{eq:chi2}) disfavors permutations that include invariant masses incompatible with the composite object's true mass. The extreme case of perfect detector resolution, vanishing losses during the clustering and absence of any other effects that cause the smearing of mass peaks serves as an illustrative example. In such a scenario, the mass distributions would be represented by sharp peaks around the particle's true mass value. If the decay products are present among the selected objects, the $\chi^2$ method will automatically find the correct configuration because even slight disagreements between $m_{p,k}$ and $\hat{m}_k$ are penalized through that score. \newline
Tables \ref{table:RecoEffs4JetsRegressed} through \ref{table:RecoEffs6JetsRegressed} quantify the absolute change in the reconstruction performance of composite objects using the $\chi^2$ based method. For this, the mass parameters $\hat{m}_k$ and $\sigma_{m_k}$ are updated to the values in table \ref{table:massResolutions}. Otherwise, the procedure as outlined in chapter $\ref{sec:5.3}$ is unchanged.

\begin{table}[H]
	\caption{Absolute change in $\chi^2$ based reconstruction efficiencies for $N_{Jet}$=4 with respect to table \ref{table:RecoEffs4Jets}. Numbers are given in \%.}
	\centering
	\begin{tabular}{l|c|c|c|c|c|c}
		& $\boldsymbol{t_{lep}}$ & $\boldsymbol{W_{had}}$ & $\boldsymbol{H}$ & $\boldsymbol{t_{had}}$ & $\boldsymbol{W_{had}+H}$ & $\boldsymbol{t_{had}}+\boldsymbol{W_{had}}+\boldsymbol{t_{lep}}$\\
		\hline
		\midrule
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h1} & \cellcolor{YellowGreen}+0.5 & - & - & - & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h2} & - & \cellcolor{YellowGreen}+0.3 & - & -  & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h3} & - & - & \cellcolor{YellowGreen}+1.1 & - & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h4} & - & 0.0 & - & \cellcolor{YellowGreen}+0.9 & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h5} & 0.0 & \cellcolor{Red}-0.1 & - & - & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h6} & \cellcolor{YellowGreen}+0.3 & - & \cellcolor{YellowGreen}+0.7 & - & - & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h7} & - & \cellcolor{Red}-1.0 & \cellcolor{YellowGreen}+0.5 & - & \cellcolor{YellowGreen}+0.4 & -\\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h8} & \cellcolor{Red}-0.1 & \cellcolor{Red}-0.2 & - & \cellcolor{YellowGreen}+0.3 & - & \cellcolor{YellowGreen}+0.3 \\
	\end{tabular}
	\label{table:RecoEffs4JetsRegressed}
\end{table}
In the $N_{Jet}=4$ bin minor improvements are observed overall at the cost of decreased reconstruction efficiency for the $W_{had}$ in hypotheses in which multiple objects are targeted. It is attributed to the scenario in which jets from the true $W$ boson decay are b-tagged by chance and are corrected towards higher energies by the regression. Instead of being assigned to either $lj1$ or $lj2$, these jets are now attributed to a bottom flavor from either the top quarks or the Higgs boson. By contrast, the true configurations of the latter are correctly identified more often among all hypotheses. 
\begin{table}[H]
	\caption{Absolute change in $\chi^2$ based reconstruction efficiencies for $N_{Jet}$=5 with respect to table \ref{table:RecoEffs5Jets}. Numbers are given in \%.}
	\centering
	\begin{tabular}{l|c|c|c|c|c|c}
		& $\boldsymbol{t_{lep}}$ & $\boldsymbol{W_{had}}$ & $\boldsymbol{H}$ & $\boldsymbol{t_{had}}$ & $\boldsymbol{t_{lep}}+\boldsymbol{W_{had}+H}$ & $\boldsymbol{t_{had}}+\boldsymbol{W_{had}}+\boldsymbol{H}$\\
		\hline
		\midrule
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h1} & \cellcolor{YellowGreen}+0.3 & - & - & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h2} & - & \cellcolor{YellowGreen}+0.1 & - & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h3} & - & - & \cellcolor{YellowGreen}+1.5 & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h4} & - & \cellcolor{Red}-0.1 & - & \cellcolor{YellowGreen}+1.0 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h5} & \cellcolor{YellowGreen}+0.1 & 0.0 & - & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h6} & \cellcolor{YellowGreen}+0.5 & - & \cellcolor{YellowGreen}+1.2 & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h7} & - & \cellcolor{Red}-0.7 & \cellcolor{YellowGreen}+0.8 & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h8} & \cellcolor{YellowGreen}+0.3 & \cellcolor{Red}-0.1 & - & \cellcolor{YellowGreen}+0.8 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h9} & \cellcolor{Red}-0.1 & \cellcolor{Red}-1.0 & \cellcolor{YellowGreen}+0.4 & - & \cellcolor{YellowGreen}+0.1 & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h10} & - & \cellcolor{Red}-0.1 & \cellcolor{Red}-0.1 & \cellcolor{YellowGreen}+0.5 & - & \cellcolor{YellowGreen}+0.9\\
	\end{tabular}
	\label{table:RecoEffs5JetsRegressed}
\end{table}
Similarly, the same statements hold for the $N_{Jet}=5$ bin. Here, the Higgs boson efficiency is increased by an absolute of 1.5\% when attempting to reconstruct it only (h3). The hadronically decaying top quark is identified more often at an increased efficiency of absolute 1.0\%. \newline\newline
$N_{Jet}\geq 6$ events profit most from the additional energy correction in terms of reconstruction efficiencies. Higgs bosons are identified more effectively for all underlying hypotheses. The efficiencies for the top quarks are improved and the $W_{had}$ is reconstructed at comparable efficiencies with respect to the case without applied regression. 
\begin{table}[H]
	\caption{Absolute change in $\chi^2$ based reconstruction efficiencies for $N_{Jet}\geq$6 with respect to table \ref{table:RecoEffs6Jets}. Numbers are given in \%.}
	\centering
	\begin{tabular}{l|c|c|c|c|c}
		& $\boldsymbol{t_{lep}}$ & $\boldsymbol{W_{had}}$ & $\boldsymbol{H}$ & $\boldsymbol{t_{had}}$ &  $\boldsymbol{t_{had}}+\boldsymbol{W_{had}}+\boldsymbol{H}+\boldsymbol{t_{lep}}$\\
		\hline
		\midrule
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h1} & \cellcolor{YellowGreen}+0.5 & - & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h2} & - & 0.0 & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h3} & - & - & \cellcolor{YellowGreen}+1.5 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h4} & - & \cellcolor{YellowGreen}+0.2 & - & \cellcolor{YellowGreen}+1.0 & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h5} & \cellcolor{YellowGreen}+0.4 & 0.0 & - & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h6} & \cellcolor{YellowGreen}+0.4 & - & \cellcolor{YellowGreen}+1.2 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h7} & - & 0.0 & \cellcolor{YellowGreen}+1.4 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h8} & \cellcolor{YellowGreen}+0.4 & \cellcolor{YellowGreen}+0.1 & - & \cellcolor{YellowGreen}+0.8 & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h9} & \cellcolor{YellowGreen}+0.4 & \cellcolor{Red}-0.2 & \cellcolor{YellowGreen}+1.0 & - & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h10} & - & 0.0 & \cellcolor{YellowGreen}+1.0 & \cellcolor{YellowGreen}+0.7 & - \\
		$\boldsymbol{\Delta\epsilon}:~$\textbf{h11} & \cellcolor{YellowGreen}+0.5 & 0.0 & \cellcolor{YellowGreen}+0.8 & \cellcolor{YellowGreen}+0.4 & \cellcolor{YellowGreen}+0.8 \\
	\end{tabular}
	\label{table:RecoEffs6JetsRegressed}
\end{table}

It has been motivated in section \ref{sec:5.1} that an effective event reconstruction of the $t\bar{t}H(b\bar{b})$ system is expected to provide well discriminating variables for the signal to background separation in the statistical analysis (section \ref{sec:8.3}).