\Section{Mass Resolutions of Higgs and W Bosons}\label{sec:mass_resolutions_of_higgs_and_w}%

\Subsection{Motivation}\label{subsec:motivation_res}%
As explained in section \ref{sec:jet_grooming_algorithms}, grooming methods are designed to clean up jets from pile up or underlying events by removing rather soft constituents at large angles. Due to this removal of some parts, we anticipate a general shift of the invariant mass peaks to lower values after the application of grooming. In addition, if only entities are discarded that do not belong to the actual physical process, the width of the distributions supposedly decreases. These two effects are accounted for by the introduction of a resolution, which shall be defined as the quotient between a measure of width and some mean value of the distribution. In this analysis, the effect of jet grooming on this particular mass resolution of reconstructed W and Higgs bosons is investigated.

\Subsection{Measurement Strategy}\label{subsec:strategy}%
For the purpose of calculating the invariant masses, the reconstructed jets first need to be identified to match the partons from whose decay they originate. Since the given samples in this thesis are simulated, the generator information is available. Thus, the jet-parton association is achieved by comparison of angular distances between reconstructed jet i and generated parton j ($\rightarrow$ eq. \ref{measure2}). If $\Delta_{ij}$ is below the value of 0.3, jet i is identified to belong to parton j. Yet, if one reconstructed object may be associated to multiple partons, then it is called to be "merged". \newline \noindent\hspace*{5mm} In order to reconstruct a Higgs boson within an event, the bottom and antibottom quarks from the generated Higgs decay must have been matched to jets. In case this is given, the four-vectors of the respective jets, labeled "b\_H" and "bbar\_H", are added and the mass of the Higgs is computed according to the relativistic energy-momentum relation (eq. \ref{mpE}).
\begin{equation}
m_{H}^2~=~\left(E_{b\_H}+E_{bbar\_H}\right)^2~-~\left(\vec{p}_{b\_H}+\vec{p}_{bbar\_H}\right)^2
\label{mpE}
\end{equation}
The steps for the calculation of the W boson's mass are similar. The difference is the requirement of two light jets "j1\_t" and "j2\_t" from the W$^+$, or the "j1\_tbar", "j2\_tbar" from W$^-$ decay. At this point, there is no distinction between W$^-$ and W$^+$. Instead both particles are simply considered as W bosons.\newline \noindent\hspace*{5mm} After this procedure is repeated for all events and the invariant masses are filled in respective histograms, multiple gaussian fits to the resulting distributions are performed. Their range is set within the interval in which the graph is dropped to approximately half of its peak height. Since the fit results strongly depend on the specified range, its upper and lower limit are perturbated N = 100 times by shifting their absolute values individually by a gaussian distributed number with $\mu_{p}$ = 0 and $\sigma_{p}$ = 5. For each run, the fit is repeated and the obatined values for $\mu~\pm~s_{\mu}$ and $\sigma~\pm~s_{\mu}$ are saved. Afterwards, weighted means are calculated with equations \ref{mean_mean} and \ref{mean_sigma}.

\begin{equation}
\overline{\mu}~=~\frac{\sum_{i=1}^{N}\frac{\mu_i}{s_{\mu,i}^2}}{\sum_{i=1}^{N}\frac{1}{s_{\mu,i}^2}}
\label{mean_mean}
\end{equation}
\begin{equation}
\overline{\sigma}~=~\frac{\sum_{i=1}^{N}\frac{\sigma_i}{s_{\sigma,i}^2}}{\sum_{i=1}^{N}\frac{1}{s_{\sigma,i}^2}}
\label{mean_sigma}
\end{equation}
The respective errors are given by equations \ref{err1} and \ref{err2}
\begin{equation}
s_{\overline{\mu}}~=~\sqrt{\frac{1}{\sum_{i=1}^{N}\frac{1}{s_{\mu,i}^2}}}\cdot\sqrt{\frac{1}{N-1}\cdot\sum_{i=1}^{N}\left(\frac{\mu_i-\overline{\mu}}{s_{\mu,i}}\right)^2}
\label{err1}
\end{equation}
\begin{equation}
s_{\overline{\sigma}}~=~\sqrt{\frac{1}{\sum_{i=1}^{N}\frac{1}{s_{\sigma,i}^2}}}\cdot\sqrt{\frac{1}{N-1}\cdot\sum_{i=1}^{N}\left(\frac{\sigma_i-\overline{\sigma}}{s_{\sigma,i}}\right)^2}
\label{err2}
\end{equation}
Finally, the resolutions are computed using the definition $r~=~\overline{\sigma}~/~\overline{\mu}$. The errors are computed using gaussian error propagation.\newline\newline \noindent\hspace*{5mm}
In the following, r is calculated for different particle selections. On the one hand, the reconstructed particles may be required to have certain transverse momenta (p$_t$), on the other hand one could introduce a veto against merged particles meaning jets are ignored that can be associated to more than one generator parton. Hence, the combination of all aspects yields a total of four different perspectives under which the mass resolutions of groomed and ungroomed jets are compared in section \ref{subsec:results}.

\Subsection{Results}\label{subsec:results}%
%
%No merge veto, all pt's
%
Figures \ref{Higgs_mass_no_veto} and \ref{W_mass_no_veto} show the invariant mass distributions of reconstructed Higgs and W bosons when the merger veto is not applied and the particles are only required to have p$_t~>~$ 20 GeV. The numbers aside by the graphs indicate the corresponding resolutions after the series of fits.
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_Higgs_ttH_no_merge_veto_20-9999GeV_AK5.png}
\caption{Invariant mass distribution of reconstructed Higgs boson for different grooming methods. The merger veto is turned off.}
\label{Higgs_mass_no_veto}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_W_boson_ttH_no_merge_veto_20-9999GeV_AK5.png}
\caption{Invariant mass distribution of both reconstructed W bosons combined for different grooming methods. The merger veto is turned off.}
\label{W_mass_no_veto}
\end{figure}
In both histograms, the expected shift of the peak to lower values after the application of grooming methods is observed. Yet, for mass drop filtering it is very distinct, so that the resulting resolution is much worse than to the one yielded by the use of ungroomed jets. Considering trimming results in an absolute improvement of 0.4 \% for the Higgs boson mass and even 1.0 \% for the W boson mass.
%
%No merge veto, pt intervalls (mAss Drop not shown because way too high), explain pt binning
%
\newline\newline \noindent\hspace*{5mm}The findings for the restriction of the particles' transverse momenta (p$_t$) into different intervals is shown in figures \ref{Higgs_resolutions_no_veto} and \ref{W_resolutions_no_veto}. 
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=220px]{figures/c6/mass_resolution_no_merge_veto_H_ttHiggs.png}
\caption{Resolutions of Higgs' mass peaks for different p$_t$ intervals. The merger veto is turned off. The values and their errors are calculated according to equations \ref{mean_mean}-\ref{err2}.}
\label{Higgs_resolutions_no_veto}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=220px]{figures/c6/mass_resolution_no_merge_veto_W_t_ttHiggs.png}
\caption{Resolutions of W's mass peaks for different p$_t$ intervals. The merger veto is turned off. The values and their errors are calculated according to equations \ref{mean_mean}-\ref{err2}.}
\label{W_resolutions_no_veto}
\end{figure}
Here, mass drop filtering is not included because its resolutions are much higher and would lead to unclear scaling of the y-axis. One sees that trimmed jets lead to slightly better resolutions for reconstructed particles with transverse momenta between 20 - 100 GeV. However for higher p$_t$, there is no reliable tendency observable with respect to a decrease in mass resolutions when applying jet grooming. Nevertheless, jet pruning rather worsens the resolution which is obvious by realizing that its respective entries are always located higher in the graph than the others. Furthermore, it is worth noticing at this point that the sudden increase in the W mass resolution in the last p$_t$ bin might be caused by the lack of statistics. Namely, the number of W bosons satisfying the p$_t$ cut drops immensely. This leads to irregulary shaped distributions and consequently to non-reliable values.  Despite that, the resolutions tend to improve for higher values of p$_t$. For example, while the mass resolution of reconstructed Higgs bosons having p$_t~\in$ [20 GeV, 100 GeV] is approximately 16.5 \%, it drops to values between 10 - 11 \% when restricting it to p$_t~\in$ [300 GeV, 350 GeV]. This observation is in good agreement 
with the anticipated percental errors in the CMS calorimeters. Hereby, they tend to decrease with increasing energy of the particles ($\rightarrow$ eq. \ref{CMS:Ecal_error}).
\begin{figure}[H]
\begin{equation}
\left(\frac{\sigma_E}{E}\right)^2~=~\left(\frac{2.8\%}{\sqrt{E/GeV}}\right)^2~+~\left(\frac{12.0\%}{E/GeV}\right)^2~+~\left(0.3\%\right)^2
\label{CMS:Ecal_error}
\end{equation} 
\caption{Typical relative energy error term for the ECAL in CMS \cite{CMS:Ecal}.}
\end{figure}
Since an increase in transverse momentum often implies an increase in energy, the relative errors of the measurements drop giving sharper peaks in reconstructed mass distributions.
%
%show effect of merger veto to the invariant mass distributions
%
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/TTH_merge_veto_Higgs.png}
\caption{Invariant mass distributions for reconstructed Higgs with (=filtered) and without (=unfiltered) merger veto.}
\label{Higgs_veto_on}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/TTH_merge_veto_W.png}
\caption{Invariant mass distributions for reconstructed W bosons with (=filtered) and without (=unfiltered) merger veto.}
\label{W_veto_on}
\end{figure}

Figures \ref{Higgs_veto_on} and \ref{W_veto_on} give an approximate idea on how the merger veto affects the shapes of mass distributions. In both plots it is exemplary shown for ungroomed jets that the resolutions is slightly enhanced due to the disappearence of the distribution tail for higher masses.\newline \noindent\hspace*{5mm}This impression is confirmed by the corresponding distributions with included merger veto as depicted in figures \ref{Higgs_mass_with_veto} and \ref{W_mass_with_veto}. There the values drop by approximately a few per mille in agreement to the just recognized vanished tail.
%
%With merger veto, all pt's
%
\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_Higgs_ttH_with_merge_veto_20-9999GeV_AK5.png}
\caption{Invariant mass distribution of reconstructed Higgs boson for different grooming methods. The merger veto is turned on.}
\label{Higgs_mass_with_veto}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_W_boson_ttH_with_merge_veto_20-9999GeV_AK5.png}
\caption{Invariant mass distribution of both reconstructed W bosons combined for different grooming methods. The merger veto is turned on.}
\label{W_mass_with_veto}
\end{figure}

%
%With merger veto, pt intervalls
%

\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_resolution_with_merge_veto_H_ttHiggs.png}
\caption{Resolutions of Higgs' mass peaks for different p$_t$ intervals. The merger veto is turned on. The values and their errors are calculated according to equations \ref{mean_mean}-\ref{err2}.}
\label{Higgs_resolutions_with_veto}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1.15 \textwidth,height=230px]{figures/c6/mass_resolution_with_merge_veto_W_t_ttHiggs.png}
\caption{Resolutions of W's mass peaks for different p$_t$ intervals. The merger veto is turned on. The values and their errors are calculated according to equations \ref{mean_mean}-\ref{err2}.}
\label{W_resolutions_with_veto}
\end{figure}

Apart from this slight enhancement and an overall loss of statistics by approximately 20 \% due to the veto, the values for different p$_t$ intervals as illustrated in figures \ref{Higgs_resolutions_with_veto} and \ref{W_resolutions_with_veto} do not differ significantly and thus imply the same conclusions like the previous plots (fig. \ref{Higgs_resolutions_no_veto} and \ref{W_resolutions_no_veto}). 
