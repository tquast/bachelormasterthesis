\Section{Theory}\label{sec:theory}%
\Subsection{The Standard Model of Particle Physics}\label{subsec:the_standard_model_of_particle_physics}%
The Standard Model of Particle Physics (SM) represents our current state of knowledge on how matter is constructed and how the interactions emerge. The Higgs mechanism is one aspect to it which describes the origin of mass by introducing a scalar Higgs field. The Higgs boson predicted by this theory is discussed more detailly in section \ref{subsec:the_higgs_boson}.\newline \noindent\hspace*{5mm}
While gravitiy is not part of the Standard Model, the other interactions are included and can be formulated within quantum field theories. Therein, fundamental forces are mediated by the exchange of particles with an integer spin of one, so called "gauge bosons".\newline \noindent\hspace*{5mm}
The longest known interaction besides gravity is electromagnetism. Its phenomena are described in the theory of quantum electrodynamics. The carriers for the electromagnetic force are photons ($\gamma$). They only couple to electrically charged particles and have a rest mass of zero. As a consequence of the latter, photons propagate with the speed of light and the Coulomb potential has infinite range.\newline \noindent\hspace*{5mm}
A tremendous achievement of the last century in theoretical physics was the unification of electromagnetism with the weak force. Its gauge bosons are the massive W$^{\pm}$ and the Z bosons. The $\pm$ indicates that W bosons have an electric charge of either plus or minus one times the elementary charge e. Thus, they are also subject to the laws of electromagnetism. Due to the high masses of the gauge bosons, its range is only in the order of ~10$^{-18}$ m  \cite{SM:Collider}. In addition, weak interaction is also special since it violates parity symmetry. The latter has experimentally been observed for neutrinos originating from $\beta$-decays whose helicity is either left- or right-handed depending on the underlying process \cite{parity_violation}.\newline \noindent\hspace*{5mm}
Finally, there is the strong nuclear interaction described by the theory of quantum chromodynamics. It introduces color charges as additional quantum numbers and predicts the existence of eight gluons propagating the corresponding strong force. Although gluons do not have rest mass like photons, the range of this interaction is limited to approximately 10$^{-15}$ m. According to the concept of color confinement, the strong nuclear force between two particles does not vanish with increasing distance but tends to a constant. Eventually, the potential reaches a point at which massive particles can emerge from the vacuum. Consequently, the pion's mass, the lightest possible mass for particles, sets the upper limit for the range of strong interaction.\newline \noindent\hspace*{5mm}
Table \ref{table:gauge_bosons} gives an overview of the interactions and its gauge bosons.
\begin{table}[H]
\centering
\begin{tabular}{ccccc}%
\toprule%
\textbf{Interaction} &\textbf{Gauge boson} & \textbf{Mass [GeV]} & \textbf{El. charge [e]}\\%
\midrule%
EM & photon & 0 & 0 \\
\multirow{2}{*}{weak} & W$^{\pm}$& 80.4 & $\pm$ 1\\
& Z & 91.2 & 0 \\
strong & gluons & 0 & 0\\
\bottomrule%
\end{tabular}%
\caption{Fundamental interactions and their force mediating bosons in the Standard Model \cite{PDG}.}
\label{table:gauge_bosons}
\end{table}
Besides these force carrying bosons, the Standard Model contains a set of fermionic particles that can be subdivided into quarks and leptons. There are three generations for both groups. Furthermore, each particle has a contrary partner, an antiparticle, with the same mass but opposite quantum numbers.\newline \noindent\hspace*{5mm}
Six different types of quarks exist within the Standard Model, namely the up (u), down (d), charm (c), strange (s), bottom (b) and top (t) quark plus their antiquarks. All of them are subject to all fundamental forces and cannot be observed isolated due to color confinement. Instead, they quickly hadronize, neutralizing their color charge and forming hadrons, i.e. groups of two (mesons) or three (baryons) quarks. \newline \noindent\hspace*{5mm} On the contrary, leptons are mainly subject to weak interaction. Nonetheless, the electrically charged electron (e), muon ($\mu$) and the heavy tau ($\tau$) lepton are also affected by electromagnetic forces. The remaining three leptons are the very light and uncharged neutrinos $\nu_e$, $\nu_{\mu}$ and $\nu_{\tau}$. The indices indicate with whom the neutrino forms a generation of leptons. Since they only interact weakly with matter, neutrinos are very hard to detect. \newline \noindent\hspace*{5mm}
An overview with all SM fermions and their masses is given in table  \ref{table:fermions}.
\begin{table}[H]
\centering
\begin{tabular}{cccccccc}%
\toprule%
\multirow{2}{*}{\textbf{Type}} & \multirow{2}{*}{\textbf{Gen.}} & \multirow{2}{*}{\textbf{Name}} & \multirow{2}{*}{\textbf{Symbol}} & \textbf{Charge} & \multicolumn{3}{c}{\textbf{Interactions}}\\%
& & & & \textbf{(el.) [e]} & \textbf{EM} & \textbf{weak} & \textbf{strong}\\%
\midrule%
\multirow{2}{*}{} & \multirow{2}{*}{I} & up & $u$ & 2/3 & \ding{51} & \ding{51} & \ding{51}\\%
& & down & $d$ & -1/3\ \ & \ding{51} & \ding{51} & \ding{51}\\%
\cmidrule{2-8}%
\multirow{2}{*}{quark} & \multirow{2}{*}{II} & charm & $c$ & 2/3 & \ding{51} & \ding{51} & \ding{51}\\%
& & strange & $s$ & -1/3\ \ & \ding{51} & \ding{51} & \ding{51}\\%
\cmidrule{2-8}%
\multirow{2}{*}{} & \multirow{2}{*}{III} & top & $t$ & 2/3 & \ding{51} & \ding{51} & \ding{51}\\%
& & bottom & $b$ & -1/3\ \ & \ding{51} & \ding{51} & \ding{51}\\%
\midrule%
\multirow{2}{*}{} & \multirow{2}{*}{I} & electron neutrino & $\nu_e$ & 0 & & \ding{51} &\\%
& & electron & $e$ & -1\ \ & \ding{51} & \ding{51} &\\%
\cmidrule{2-8}%
\multirow{2}{*}{lepton} & \multirow{2}{*}{II} & muon neutrino & $\nu_\mu$ & 0 & & \ding{51} &\\%
& & muon & $\mu$ & -1\ \ & \ding{51} & \ding{51} &\\%
\cmidrule{2-8}%
\multirow{2}{*}{} & \multirow{2}{*}{III} & tau neutrino & $\nu_\tau$ & 0 & & \ding{51} &\\%
& & tau lepton & $\tau$ & -1\ \ & \ding{51} & \ding{51} &\\%
\bottomrule%
\end{tabular}%
\caption{Fermions and their masses within the Standard Model \cite{MT:Rieger}. }
\label{table:fermions}
\end{table}
The top quark is a very remarkable fermion owing to its high mass that is comparable to the one of a gold atom. For this reason, the top quark is involved in many high-order SM processes including those related to Higgs boson production and decay.

\Subsection{The Higgs Boson}\label{subsec:the_higgs_boson}%
	\Subsubsection{Higgs Boson Properties}\label{subsubsec:higgs_boson_properties}
	The SM Higgs boson is an electrically neutral particle with an integer spin of zero. It can be understood as an excitement of the Higgs field giving mass to particles. In particular, the Standard Model would lack an explanation for the masses of W and Z bosons if it was not extended by the Higgs mechanism in 1964 \cite{Higgs_mechanism}. The experimental observation of the Higgs boson requires both high center-of-mass energies and high luminosities in particle colliders. Hence, it was a magnificent success when CMS and ATLAS simultaneously reported the observation of a possible Higgs candidate with an invariant mass between 125 - 126 GeV \cite{ATLAS:Higgs_detection, CMS:Higgs_detection}. 
	\begin{figure}[H]
	\begin{minipage}{7cm}
	\centering
	\includegraphics[height = 150px, width = 200px]{figures/c2/Higgs_mass_CLs.png}
	\label{Higgs_detection1}
	\end{minipage}
	\hfill
	\begin{minipage}{7cm}
	\centering
	\includegraphics[height = 150px, width = 180px]{figures/c2/Higgs_local_p_value.png}
	\label{Higgs_detection1}
	\end{minipage}
	\caption{Higgs mass confidence levels for the combination of all decay channels (left) and local p-values (right) reported by CMS in 2012 \cite{CMS:Higgs_detection}.}
	\end{figure}
	While its physical properties that have been determined experimentally so far are in good agreement with the Standard Model predictions, precise decay rates of this candidate remain to be measured and to be compared to theory \cite{CERN:decay_rates}. 
	
	\Subsubsection{ttH Production and Decay}\label{subsubsec:tth_production_and_decay_channel}%
	In proton-proton collisions, Higgs bosons are mainly produced in five processes whose cross sections are dominated by the gluon-gluon fusion with $\sigma_{ggf}$ = 19.27 pb at $\sqrt{s}$ = 8 TeV and m$_H$ = 125.0 GeV \cite{Higgs:properties}. This thesis deals with the Higgs production in association with top quarks. Although the cross section $\sigma_{t\bar{t}H}$ = 0.1293 pb \cite{Higgs:properties} is much smaller in comparison to the other four processes, the coupling of the Higgs boson to fermions needs to be analysed in more detail.	Due to the proportionality of the coupling constant to the fermion's mass, the interaction between Higgs bosons and massive top quarks (table \ref{table:fermions}) is a very promising production channel for such analyses. \newline Figure \ref{ttH_semileptonic} shows a possible Feynman graph illustrating the $t\bar{t}H$ process. 	
	\begin{figure}[H]
	\centering
	\includegraphics[width=220px,height=160px]{figures/c2/ttH_semileptonic.png}
	\caption{Semileptonic $t\bar{t}H$ event \cite{MT:Rieger}. Please note, that this thesis considers the hadronic decay of both W bosons.}
	\label{ttH_semileptonic}
	\end{figure}
	Furthermore, in the investigated process at hand, the Higgs boson   decays into a pair of bottom quarks. The branching ratio for $H\rightarrow b\bar{b}$ for the hypothesis m$_H$ = 125 GeV is the highest among the other possible Higgs decays (figure \ref{Higgs_BR}). Given the branching ratio $BR_{H\rightarrow b\bar{b}}$ = 0.577 \cite{Higgs:properties} and the cross section for $t\bar{t}H$ under consideration that the W bosons shall decay hadronically ($BR_{W\rightarrow qq}$ = 0.6760 \cite{PDG}), the overall cross section yields  $\sigma_{t\bar{t}H}~\times~BR_{H\rightarrow b\bar{b}}~\times~\left(BR_{W\rightarrow qq}\right)^2$ = 34.09 fb. However, the cross section for $t\bar{t}b\bar{b}$ processes has been measured to $\sigma_{t\bar{t}b\bar{b}}~\approx$ 278 fb \cite{CMS:ttbb}. The final state of these events is very similar to the $t\bar{t}H$ channel thus representing irreducible background.
		
	\begin{figure}[H]
		\centering
		\includegraphics[width=195px,height=140px]{figures/c2/Higgs_BR.png}
		\caption{Higgs branching ratios for low Higgs mass hypotheses \cite{Higgs:properties}.}
		\label{Higgs_BR}
	\end{figure}
	
	Throughout this thesis, the following naming convention is applied. The two bottom quarks originating from the Higgs decay are labeled "b\_H" or "bbar\_H". "b\_t" and "b\_tbar" indicate the bottom quarks from the top and antitop decays, respectively. Finally, the light jets from the W boson decays are labeled "j1\_t" and "j2\_t" for the branch with the top quark or "j1\_tbar" and "j2\_tbar" for the antitop quark branch. Hereby, the index "1" stands for the particle with smaller transverse momentum.
%	