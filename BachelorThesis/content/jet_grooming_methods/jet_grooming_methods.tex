\Section{Jet Grooming}\label{sec:jet_grooming_algorithms}%
\Subsection{Jet Clustering Algorithms}\label{subsec:jet_clustering_algorithms}%
Due to their short lifetime as well as because of her decay and production close to the primary interaction point, most partons cannot be observed directly in particle detectors. Instead, after hadronization and fragmentation processes, stable hadrons are created leaving their signatures ($\rightarrow$ figure \ref{Jets_detector}). 
\begin{figure}[H]
\centering
\includegraphics[width=150px,height=180px]{figures/c4/detector_jets.png}
\caption{Illustration of jets in a particle detector\cite{Schieferdecker:Jet_Algorithms}.}
\label{Jets_detector}
\end{figure}
The goal of \textit{jet clustering} is the combination of these hadrons to collimated objects, so called \textit{jets}. Hence, it represents an approximate attempt to reverse-engineer the effects of hadronization and fragmentation \cite{Schieferdecker:Jet_Algorithms}. \newline \noindent\hspace*{5mm} Although there is not one universal procedure that is generally accepted, the sequential clustering algorithms, in particular the k$_t$, the Cambridge-Aachen (C/A) and the anti - k$_t$ algorithms, are mainly applied at hadron-hadron colliders \cite{ATLAS:grooming_investigations}. This relies on their simplicity and on the fact that they are both infrared- and collinear- safe, i.e. the jet definition is insensitive to soft radiation and underlying underground processes \cite{Schieferdecker:Jet_Algorithms}. \newline \noindent\hspace*{5mm}
For each of the three methods, a measure of distance  $d_{ij}$,  between entity i and j, being either particles or pseudojets, is introduced as in equations \ref{measure1} and \ref{measure2} \cite{anti_kt}.
\begin{equation}
d_{ij}~:=~\min{\left(k_{ti}^{2p},k_{tj}^{2p}\right)}\cdot\frac{\Delta_{ij}^2}{R^2}
\label{measure1}
\end{equation}
\begin{equation}
\Delta_{ij}^2~:=~\left(y_i~-~y_j \right)^2~+~\left(\phi_i~-~\phi_j \right)^2 
\label{measure2}
\end{equation}
Variables with the indices representing entities are defined as follows: 
\begin{itemize}
\item $k_{t}$: transverse momentum [GeV]
\item $y$: rapidity 
\item $\phi$: azimuth angle
\end{itemize}
The parameter R represents a measure for the cone of the resulting jets. Principally, p can be chosen arbitrarily. Nonetheless, +1, 0 and -1 are customary values for p to recover the k$_t$, the C/A and the anti - k$_t$ algorithms, respectively. In each step, $d_{ij}$ is calculated for all pairs of entities. Afterwards, the smallest distance $d_{min}$ is identified. If $d_{min}$ is also smaller than every $k_{ti}^{2p}$, the two objects making $d_{min}$ are clustered and form a new entity by adding their four-vectors. In case the minimal $k_{ti}^{2p}$ is smaller than $d_{min}$, entity i is called a jet and not considered any further. Then this procedure is iterated until all entities have been treated. \newline \noindent\hspace*{5mm} Besides a few exceptions, e.g. for jet grooming, CMS has adopted the anti - k$_t$ algorithm with p = -1 as standard and chooses R = 0.5 or 0.7 for most analyses \cite{anti_kt_at_CMS}. While the k$_t$ and C/A - algorithms are hierarchical in relative transverse momentum or in angle, the anti - k$_t$ is designed to give perfectly conical hard jets \cite{anti_kt_at_CMS}. This is due to the negative exponent of $k_{ti}$ yielding small values of $d_{ij}~~\forall~j$ if entity i has high transverse momentum. It causes i to be clustered first to its softer (less k$_t$) neighbours before other hard entities are even considered. As an example, the different outcomes after the application of the k$_t$ and the anti - k$_t$ on the same event is depicted in figure \ref{anti_kt}. 

\begin{figure}[H]
\begin{minipage}{7cm}
\centering
\includegraphics[width=211px,height=140px]{figures/c4/kt.png}
\end{minipage}
\hfill
\begin{minipage}{7cm}
\centering
\includegraphics[width=211px,height=140px]{figures/c4/anti_kt.png}
\end{minipage}
\caption{Clustered jets after the application of the k$_t$ (left) and anti - k$_t$ algorithm (right) \cite{anti_kt}.}
\label{anti_kt}
\end{figure}

Both jet clustering and jet grooming (see section \ref{subsec:grooming_methods}) of raw data at CMS is implemented via the software package FASTJET \cite{CMS:Jet_substructure}. Within the samples used in this thesis, these jet finding and grooming algorithms as wells as various dedicated corrections have already been applied. Throughout the upcoming sections, the naming convention according to table \ref{jet_naming} is used.

\begin{table}[H]
\centering
\begin{tabular}{c||c|c|c}
\textbf{Name} & \textbf{Algorithm} & \textbf{R} & \textbf{Reference section} \\ 
\toprule
\textbf{Jet} & Anti - k$_t$& 0.5 & \ref{subsec:jet_clustering_algorithms} \\
\textbf{AK5Pruned} & Pruning & 0.5 & \ref{subsubsec:pruning} \\
\textbf{AK5Trimmed} & Trimming & 0.5 & \ref{subsubsec:trimming} \\
\textbf{AK5MassDrop} & Mass Drop Filtering & 0.5 & \ref{subsubsec:mass_drop_filtering}\\
\end{tabular}
\caption{Naming convention for jet objects in this thesis. R represents the according parameter for clustering of the initial jets. The "AK" indicates that the intital jets prior to grooming have been clustered with the anti - k$_t$ algorithm.}
\label{jet_naming}
\end{table}

\Subsection{Grooming Methods}\label{subsec:grooming_methods}%
In general, jet grooming can be thought of as an additional clean up of jet constituents.
Their substructure reveals relevant physics in an event that is often hidden due to soft emissions, underlying events and pile up. Though the latter is partially corrected for by looking at the tracks and vertices in the detector \cite{Vos:jet_substructure}, pile up is still a problem. In principle, jet grooming addresses it by removing soft jet parts at large angles leaving constituents of interest from the hard scatter behind \cite{CMS:Jet_substructure}. \newline \noindent\hspace*{5mm} Here, three possible jet grooming methods, namely jet pruning, trimming and mass drop filtering are investigated and hence shall be explained in this section.

\Subsubsection{Pruning}\label{subsubsec:pruning}%
Jet pruning was initially suggested in \cite{Pruning}. It is designed to improve the mass resolutions of reconstructed jets in heavy particle decays. The method introduces an additional cut to kinematic variables during the reclustering of subjets within a jet using either the k$_t$ or C/A-algorithm. These cuts are considered as a criterion if the branching represents an actual decay or not. Pruning itself and the formulated cuts are depicted in figure \ref{img:Pruning}.

\begin{figure}[H]
\centering
\includegraphics[width=350px,height=100px]{figures/c4/Pruning.png}
\caption{Illustration of pruning \cite{Grooming}.}
\label{img:Pruning}
\end{figure}

At CMS, the parameters are chosen to $r_{cut}=0.5$ and $z_{cut}=0.3$. If these parameters are affected by cuts at one step,  the respective branch is unlikely to be a result from a reconstructed heavy particle decay. 
Therefore, one of the two jets to be merged, usually the one with smaller transverse momentum, is discarded at this clustering iteration. \newline \noindent\hspace*{5mm} In \cite{Pruning}, jet pruning has indeed been proven to remove constituents from underlying events and QCD radiation while improving the mass resolution of reconstructed top quarks.


\Subsubsection{Trimming}\label{subsubsec:trimming}%
Like pruning, the trimming method is based on reclustering jets with $R_{sub}~<~R$. Although the precedent clustering can be done using any sequential algorithm, it is well established that using the anti - k$_t$ for the initial clustering and the $k_t$ for the reclustering step gives the best improvements \cite{Trimming}. \newline \noindent\hspace*{5mm} The idea of trimming is the discard of reclustered jets that fail to have a minimal fraction of the total transverse momentum as it is depicted in figure \ref{img:Trimming}.
\begin{figure}[H]
\centering
\includegraphics[width=350px,height=100px]{figures/c4/Trimming.png}
\caption{Illustration of trimming \cite{Grooming}.}
\label{img:Trimming}
\end{figure}
This explains the use of the k$_t$ - algorithm in the second step. The anti - k$_t$ would create imbalanced subjets, in which the transverse momentum is not equally well shared \cite{Trimming}. By definition, this is prevented by using the k$_t$ - algorithm instead.\newline \noindent\hspace*{5mm} At CMS, trimmed jets are created using $f_{cut}~=~0.3$ and $R_{sub}~=~0.2$.

\Subsubsection{Mass Drop Filtering}\label{subsubsec:mass_drop_filtering}%
The purpose of mass drop filtering is the identification  of symmetric subjets and intends to isolate energy concentrations, in particular for the $H~\rightarrow~b\bar{b}$ decay \cite{ATLAS:grooming_investigations}.\newline \noindent\hspace*{5mm}The first step is reverting the last iteration of the C/A algorithm, that must previously be applied for reclustering the initial jet, to obtain two subjets i and j. Afterwards, a new quantity, $y_{ij}$, between both is calculated as it is denoted in equation \ref{eq:y}.
\begin{equation}
y_{ij}~=~\frac{\min{\left(\left(k_t^i\right)^2,\left(k_t^j\right)^2\right)}}{M_{jet}^2}~\times~\Delta_{ij}^2
\label{eq:y}
\end{equation}
If the two subjets fail either of the two symmetry quantifying cuts $y$ $>$ $y_{cut}$ (CMS: $y_{cut}$=0.09) or $\max{\left(m_i,m_j\right)}~<~\mu_{frac}\cdot M_{jet}$ (CMS: $\mu_{frac}~$= 0.69) then they are not considered to result from a symmetric splitting and the jet is discarded. The idea of mass drop filtering is illustrated in figure \ref{img:MassDrop}.
\begin{figure}[H]
\centering
\includegraphics[width=240px,height=100px]{figures/c4/MassDrop.png}
\caption{Illustration of mass drop filtering.}
\label{img:MassDrop}
\end{figure}

